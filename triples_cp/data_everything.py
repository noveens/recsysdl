import numpy as np
import torch
from torch.autograd import Variable

from utils import *

def load_data(hyper_params):
    
    file_write(hyper_params['log_file'], "Started reading data file")
    
    train = load_obj(hyper_params['data_base'] + 'train_tuples_100')
    val = load_obj(hyper_params['data_base'] + 'val_tuples_100')
    test = load_obj(hyper_params['data_base'] + 'test_tuples_100')
    user_hist = load_obj_json(hyper_params['data_base'] + 'user_hist')
    item_hist = load_obj_json(hyper_params['data_base'] + 'item_hist')
    
    file_write(hyper_params['log_file'], "Data Files loaded!")

    train_reader = DataReader(hyper_params, train)
    val_reader = DataReader(hyper_params, val)
    test_reader = DataReader(hyper_params, test)

    return train_reader, val_reader, test_reader, len(user_hist), len(item_hist)

class DataReader:

    def __init__(self, hyper_params, tuples):
        self.hyper_params = hyper_params
        self.tuples = tuples
        self.num_b = len(self.tuples[1])

    def iter(self):

        for i in range(self.num_b):
            x_batch = self.tuples[0][i]
            x_batch[1] = [ [ float(ii) / float(5) for ii in b ] for b in x_batch[1]]
            x_batch[4] = [ [ float(ii) / float(5) for ii in b ] for b in x_batch[4]]
            y_batch = [ float(ii) / float(5) for ii in self.tuples[1][i] ]
            yield [
                Variable(torch.cuda.LongTensor(x_batch[0])),
                Variable(torch.cuda.FloatTensor(x_batch[1])), 
                Variable(torch.cuda.FloatTensor(x_batch[2])),

                Variable(torch.cuda.LongTensor(x_batch[3])),
                Variable(torch.cuda.FloatTensor(x_batch[4])), 
                Variable(torch.cuda.FloatTensor(x_batch[5])),
                
                Variable(torch.cuda.LongTensor(x_batch[6])), 
                Variable(torch.cuda.LongTensor(x_batch[7]))
            ], Variable(torch.cuda.FloatTensor(y_batch))