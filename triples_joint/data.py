import numpy as np
import random
from tqdm import tqdm
import torch
from torch.autograd import Variable

from utils import *

'''
Used to pad/clip according to the history size

to_scatter  : array to be scattered
is_user     : boolean whether to be scattered history is user history or not
is_int      : boolean whether elements to be scattered are integers or not
hyper_params: object having all hyper parameters

returns: the padded/clipped array
'''
def scatter(to_scatter, is_int, hyper_params):
    seq_lengths = torch.cuda.LongTensor(list(map(len, to_scatter)))

    if is_int == True:
        seq_tensor = Variable(torch.zeros((len(to_scatter), seq_lengths.max()))).long().cuda()
        for idx, (seq, seqlen) in enumerate(zip(to_scatter, seq_lengths)):
            seq_tensor[idx, :seqlen] = torch.cuda.LongTensor(seq)
        return seq_tensor, seq_lengths
        # Sequences are returned not sorted acc to length because not able to do this

        seq_lengths, perm_idx = seq_lengths.sort(0, descending=True)
        seq_tensor = seq_tensor[perm_idx]
        return seq_tensor, seq_lengths
        # return pack_padded_sequence(seq_tensor, seq_lengths.cpu().numpy(), batch_first=True)

    else:
        seq_tensor = Variable(torch.zeros((len(to_scatter), seq_lengths.max()))).float().cuda()
        for idx, (seq, seqlen) in enumerate(zip(to_scatter, seq_lengths)):
            if len(seq) > 0: seq_tensor[idx, :seqlen] = torch.cuda.FloatTensor(seq)
        return seq_tensor, seq_lengths
        # Sequences are returned not sorted acc to length because not able to do this

        seq_lengths, perm_idx = seq_lengths.sort(0, descending=True)
        seq_tensor = seq_tensor[perm_idx]
        return seq_tensor, seq_lengths

    # max_rows = max(list(map(len, to_scatter)))
    # # max_rows = min(max_rows, hyper_params['max_hist_size'])
    # max_rows = max(max_rows, 1) # need atleast one
    # # print(max_rows)
    # if is_int == True:
    #     scattered = [[0] * max_rows] * len(to_scatter)
        
    #     now = 0
    #     for i in to_scatter:
    #         now2 = 0
    #         for j in i:
    #             scattered[now][now2] = int(j)
    #             now2 += 1
    #             if now2 >= max_rows: break # CAN USE A HYPER-PARAM TO CLIP
    #         now += 1
    #         # scattered[now] = int(i)
    #     return Variable(torch.cuda.LongTensor(scattered))

    # else:
    #     scattered = [[0.0] * max_rows] * len(to_scatter)
        
    #     now = 0
    #     for i in to_scatter:
    #         now2 = 0
    #         for j in i:
    #             scattered[now][now2] = float(j)
    #             now2 += 1
    #             if now2 >= max_rows: break # CAN USE A HYPER-PARAM TO CLIP
    #         now += 1
    #         # scattered[now] = int(i)
    #     return Variable(torch.cuda.FloatTensor(scattered))

'''
Used to find item history i.e users that have watched this movie before a given time

item     : item id whose history needs to be found
time     : before what time is the history needed
item_hist: object having all timestamped item histories

returns: item history ; rating of the history ; timestamps of the history
'''
def find_i_hist(item, time, item_hist):
    hist = []
    hist_rating = []
    hist_time = []
    time = float(time)

    for i in item_hist[str(item)]:
        if float(i[2]) < time:
            hist.append(i[0])
            hist_rating.append(float(i[1]) / float(5))
            hist_time.append(i[2])
        else:
            return hist, hist_rating, hist_time
    return hist, hist_rating, hist_time

'''
Main function called in the main.py file

hyper_params: object having all hyper parameters

returns: object of class DataReader for train data ; test array ; number of users ; number of items
'''
def load_data(hyper_params):
    
    file_write(hyper_params['log_file'], "Started reading data file")
    
    train = load_obj_json(hyper_params['data_base'] + 'train_triples_joint')
    test = load_obj_json(hyper_params['data_base'] + 'test_triples_joint')
    user_hist = load_obj_json(hyper_params['data_base'] + 'user_hist_triples_joint')
    item_hist = load_obj_json(hyper_params['data_base'] + 'item_hist_triples_joint')
    
    file_write(hyper_params['log_file'], "Data Files loaded!")

    train_reader = DataReader(hyper_params, train, len(user_hist), item_hist)
    test_reader = DataReader(hyper_params, test, len(user_hist), item_hist)

    return train_reader, test_reader, len(user_hist), len(item_hist)

'''
Class which manages train and test data
Has a function iter which yields the x_batch, y_batch

hyper_params: object having all hyper parameters
data        : training data
num_users   : total number of users
item_hist   : object having all item history (containing users)
'''
class DataReader:

    def __init__(self, hyper_params, data, num_users, item_hist):
        self.hyper_params = hyper_params
        self.batch_size = hyper_params['batch_size']
        self.item_hist = item_hist
        self.num_users = num_users
        self.num_items = len(item_hist)
        self.data = data

        self.number()

        self._x_batches = []
        self._y_batches = []

    '''
    Counts the number of batches
    '''
    def number(self):
        y_batch = []

        count = 0
        users_done = 0

        for user in self.data:
            if users_done > self.hyper_params['number_users_to_keep']: break

            y_batch = []
            for i in range(1, len(self.data[user])):
                for ii in range(i+1, len(self.data[user])):
                    if self.data[user][i][1] == self.data[user][ii][1]: continue
                    
                    y_batch.append(0)

                    if len(y_batch) == self.batch_size / 2:
                        count += 1
                        y_batch = []
            users_done += 1

        self.num_b = count

    '''
    Yields x_batch and y_batch at each time step

    returns: [
        x_batch_u_hist, (has items)
        x_batch_u_hist_rating, (ratings given by user to above items)
        x_batch_u_hist_time, (when did user give ratings)
        
        x_batch_i_hist, (has users)
        x_batch_i_hist_rating, (rating given to item by above users)
        x_batch_i_hist_time, (when was rating given)
        
        x_batch_user, (users)
        x_batch_item (items)
    ], y_batch (rating given by x_batch_user to x_batch_item)
    '''
    def iter(self):
        keep_chance = self.hyper_params['training_random_keeping_chance']
        users_done = 0

        for user in self.data:

            if users_done > self.hyper_params['number_users_to_keep']: break
            users_done += 1

            x_batch_u_hist, x_batch_u_hist_time, x_batch_u_hist_rating = [], [], []
            x_batch_i_hist, x_batch_i_hist_time, x_batch_i_hist_rating = [], [], []
            x_batch_user = []
            x_batch_item = []
            y_batch = []

            for i in range(1, len(self.data[user])):
                for ii in range(i+1, len(self.data[user])):

                    if self.data[user][i][1] == self.data[user][ii][1]: continue

                    random_toss = random.uniform(0, 1)
                    if random_toss > keep_chance: continue

                    u_hist, u_hist_rating, u_hist_time = [], [], []
                    u_hist_2, u_hist_rating_2, u_hist_time_2 = [], [], []

                    for j in self.data[user][:i]:
                        u_hist.append(j[0])
                        u_hist_rating.append(float(j[1]) / float(5))
                        u_hist_time.append(j[2])

                    for j in self.data[user][:ii]:
                        u_hist_2.append(j[0])
                        u_hist_rating_2.append(float(j[1]) / float(5))
                        u_hist_time_2.append(j[2])

                    i_hist, i_hist_rating, i_hist_time = find_i_hist(self.data[user][i][0], self.data[user][i][2], self.item_hist) # Has users
                    i_hist_2, i_hist_rating_2, i_hist_time_2 = find_i_hist(self.data[user][ii][0], self.data[user][ii][2], self.item_hist) # Has users

                    x_batch_u_hist.append(u_hist)
                    x_batch_u_hist_rating.append(u_hist_rating)
                    x_batch_u_hist_time.append(u_hist_time)

                    x_batch_i_hist.append(i_hist)
                    x_batch_i_hist_rating.append(i_hist_rating)
                    x_batch_i_hist_time.append(i_hist_time)

                    x_batch_user.append(int(user))
                    x_batch_item.append(int(self.data[user][i][0]))


                    x_batch_u_hist.append(u_hist_2)
                    x_batch_u_hist_rating.append(u_hist_rating_2)
                    x_batch_u_hist_time.append(u_hist_time_2)

                    x_batch_i_hist.append(i_hist_2)
                    x_batch_i_hist_rating.append(i_hist_rating_2)
                    x_batch_i_hist_time.append(i_hist_time_2)

                    x_batch_user.append(int(user))
                    x_batch_item.append(int(self.data[user][ii][0]))


                    if self.data[user][i][1] > self.data[user][ii][1]:
                        greater = 1
                    else:
                        greater = -1
                    y_batch.append(greater)



                    if len(y_batch) == self.batch_size / 2:

                        yield [
                            scatter(x_batch_u_hist, True, self.hyper_params),
                            scatter(x_batch_u_hist_rating, False, self.hyper_params),
                            scatter(x_batch_u_hist_time, False, self.hyper_params),
                            
                            scatter(x_batch_i_hist, True, self.hyper_params),
                            scatter(x_batch_i_hist_rating, False, self.hyper_params), 
                            scatter(x_batch_i_hist_time, False, self.hyper_params),
                            
                            Variable(torch.cuda.LongTensor(x_batch_user)), 
                            Variable(torch.cuda.LongTensor(x_batch_item))
                        ], Variable(torch.cuda.FloatTensor(y_batch))
                        
                        x_batch_u_hist, x_batch_u_hist_time, x_batch_u_hist_rating = [], [], []
                        x_batch_i_hist, x_batch_i_hist_time, x_batch_i_hist_rating = [], [], []
                        x_batch_user = []
                        x_batch_item = []
                        y_batch = []
