import os
import sys
import random
reload(sys)
sys.setdefaultencoding("utf8")

from hyper_params import *
from utils import *





# Calculating min and max time for scaling later
f = open(hyper_params['data_base'] + 'ratings.dat')
lines = f.readlines()

max_time, min_time = 0, 1000000000000000000

for line in lines:
	temp = map(int, line.strip().split("::"))

	max_time = max(max_time, int(temp[3]))
	min_time = min(min_time, int(temp[3]))





# Calculating user and item histories
user_hist = {}
item_hist = {}
user_hist_items = {}

for line in lines:
	temp = map(int, line.strip().split("::"))

	if temp[0] not in user_hist: user_hist[temp[0]] = []
	if temp[1] not in item_hist: item_hist[temp[1]] = []
	if temp[0] not in user_hist_items: user_hist_items[temp[0]] = set()
	
	user_hist[temp[0]].append([temp[1], temp[2], float(temp[3] - min_time) / float(max_time - min_time)])
	item_hist[temp[1]].append([temp[0], temp[2], float(temp[3] - min_time) / float(max_time - min_time)])
	user_hist_items[temp[0]].add(temp[1])





# Sorting user and item histories
for user in user_hist:
	user_hist[user].sort(key=lambda x: x[2])

for item in item_hist:
	item_hist[item].sort(key=lambda x: x[2])





# Calculating the train and test splits, along with randomly sampled negative examples
train_users = []
test_users = []
val_users = []
all_users = []

users_kept = 0

for user in user_hist:

	if len(user_hist[user]) > hyper_params['max_user_hist']: continue
	if len(user_hist[user]) < hyper_params['min_user_hist']: continue
	
	users_kept += 1

	all_users.append(user)

num_val = hyper_params['data_split'][1]
num_test = hyper_params['data_split'][2]

while len(val_users) < num_val:
	user = random.choice(all_users)
	val_users.append(user)
	all_users.remove(user)

while len(test_users) < num_test:
	user = random.choice(all_users)
	test_users.append(user)
	all_users.remove(user)

train_users = all_users

print("users_kept = " + str(users_kept))
print("train users = " + str(len(train_users)))
print("test users = " + str(len(test_users)))
print("val users = " + str(len(val_users)))





# Number items according to train
item_map = {}
count_now = 1 # Start from 1 because padding

for user in train_users:
	for item in user_hist[user]:
		if item[0] not in item_map:
			item_map[item[0]] = count_now
			count_now += 1





# Make train, test, val
test = {}
train = {}
val = {}

for user in train_users:
	train[user] = []
	for item in user_hist[user]:
		train[user].append(item_map[item[0]])

for user in test_users:
	test[user] = []
	for item in user_hist[user]:
		if item[0] in item_map:
			test[user].append(item_map[item[0]])

for user in val_users:
	val[user] = []
	for item in user_hist[user]:
		if item[0] in item_map:
			val[user].append(item_map[item[0]])






# Removing extra items from item_hist
all_items = item_hist.keys()
for item in all_items:
	if item not in item_map:
		item_hist.pop(item)





# Saving all objects
save_obj_json(train, hyper_params['data_base'] + "train_rdevooght")
save_obj_json(val, hyper_params['data_base'] + "val_rdevooght")
save_obj_json(test, hyper_params['data_base'] + "test_rdevooght")
save_obj_json(item_hist, hyper_params['data_base'] + "item_hist_rdevooght")
save_obj_json(user_hist, hyper_params['data_base'] + "user_hist_rdevooght")
