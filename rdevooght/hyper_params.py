hyper_params = {
    'data_base': '../saved_data/',
    'model_file_name': '',
    'log_file': '',
    'data_split': [-1, 100, 100], # Train : Val : Test
    'max_user_hist': 200,
    'min_user_hist': 5,
    'bptt': True,

    'augmented_loss': True,
    'gamma': 1000,
    'use_weighted_loss': False,

    'learning_rate': 0.1, # Not Using this, using default
    'epochs': 100,
    'batch_size': 1,

    'embed_size': 100,

    'rnn_size': 32,
    'rnn_layers': 1,
    'rnn_dropout': 0.2,
    'rnn_bi': False,
    'rnn_attention': False,

    'end_dropout': 0.2,

    'exploding_clip': 0.25, # Not clipping now
    'training_batch_limit': 100000,
    'testing_batch_limit': 100000,
    'batch_log_interval': 1000,

    'is_cuda': True,
    'data_parallel': False,
}

file_name  = '_lr_' + str(hyper_params['learning_rate'])
file_name += '_bptt_' + str(hyper_params['bptt'])
file_name += '_rnn_size_' + str(hyper_params['rnn_size'])
file_name += '_embed_size_' + str(hyper_params['embed_size'])
file_name += '_rnn_layers_' + str(hyper_params['rnn_layers'])
file_name += '_attention_' + str(hyper_params['rnn_attention'])
file_name += '_max_user_hist_' + str(hyper_params['max_user_hist'])
file_name += '_weighted_loss_' + str(hyper_params['use_weighted_loss'])

hyper_params['log_file'] = '../saved_logs/rdevooght_log' + file_name + '.txt'
hyper_params['model_file_name'] = '../saved_models/rdevooght_model' + file_name + '.pt'