import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable

import numpy as np
import time
import pickle
import json
import gc
import datetime as dt

# from data_everything import load_data
from data import load_data_dssm
from model_dssm import Model
from utils import *
from hyper_params_dssm import *

def find_item_hist(i_hist, item):
    hist = []

    for i in i_hist[str(item)]:
        hist.append(i[0])
        if len(hist) == hyper_params['item_hist_size']:
            return hist

    while len(hist) < hyper_params['item_hist_size']:
        hist.append(0)

    return hist

def evaluate(test, negs, history, i_hist):
    model.eval()
    total_loss = 0
    batch = 0

    ############# When to reinit hidden of the LSTM ?

    total, count = 0, 0
    mse = 0.0
    corr_eval, total_eval = 0, 0
    c_hr, t_hr = 0, 0

    for user in test:
        batch += 1
        if batch > hyper_params['testing_batch_limit']: break
        
        x_user_hist = [ history[user] ]
        x_item_hist = [ find_item_hist(i_hist, test[user][0]) ]
        y = [[1.0]]

        for neg_item in negs[user]:
            x_user_hist.append(history[user])
            x_item_hist.append(find_item_hist(i_hist, neg_item))
            y.append([0.0])

        y = Variable(torch.cuda.FloatTensor(y))

        output = model(Variable(torch.cuda.LongTensor(x_user_hist)), Variable(torch.cuda.LongTensor(x_item_hist)))
        loss = criterion(output, y)
        total_loss += loss.data

        # Evaluating Accuracy
        y = y.data
        output = output.data[:, 0]

        _, top = torch.sort(output)
        top_k = top[-10:]

        # print(output)
        # print(_)
        # print(top)
        # print(top_k)
 
        if 0 in top_k:
            c_hr += 1
        t_hr += 1
        
    return (total_loss[0]/batch) ** 0.5, (float(c_hr) / float(t_hr)) * 100.0
    # return total_loss[0]/batch, (float(corr_eval) / float(total_eval)) * 100.0

def train(reader):
    model.train()
    total_loss = 0
    start_time = time.time()
    batch = 0

    ############# When to reinit hidden of the LSTM ?

    for x_user_hist, x_item_hist, y in reader.iter():
        batch += 1
        if batch > hyper_params['training_batch_limit']: return
        
        optimizer.zero_grad()

        output = model(x_user_hist, x_item_hist)

        # print(y)
        # print(output)
        # print("\n")

        loss = criterion(output, y)
        loss.backward()

        torch.nn.utils.clip_grad_norm(model.parameters(), hyper_params['exploding_clip'])
        optimizer.step()

        total_loss += loss.data

        if batch % hyper_params['batch_log_interval'] == 0 and batch > 0:
            cur_loss = (total_loss[0] / hyper_params['batch_log_interval']) ** 0.5
            elapsed = time.time() - start_time

            ss = '| epoch {:3d} | {:5d}/{:5d} batches | ms/batch {:5.2f} | loss {:5.4f}'.format(
                epoch, batch, int(min(train_reader.num_b, hyper_params['training_batch_limit'])),
                elapsed * 1000 / hyper_params['batch_log_interval'], cur_loss)
            
            file_write(hyper_params['log_file'], ss)

            total_loss = 0
            start_time = time.time()

train_reader, test, negs, history, i_hist, total_users, total_items = load_data_dssm(hyper_params)
hyper_params['total_users'] = total_users
hyper_params['total_items'] = total_items

file_write(hyper_params['log_file'], "\n\nSimulation run on: " + str(dt.datetime.now()) + "\n\n")
file_write(hyper_params['log_file'], "Data reading complete!")
file_write(hyper_params['log_file'], "Number of train batches: {:4d}".format(train_reader.num_b))
file_write(hyper_params['log_file'], "Total Users: " + str(total_users))
file_write(hyper_params['log_file'], "Total Items: " + str(total_items) + "\n")

model = Model(hyper_params)
model.cuda()

# criterion = nn.MSELoss()
criterion = nn.BCELoss(size_average=True)

optimizer = torch.optim.Adadelta(model.parameters())

file_write(hyper_params['log_file'], str(model))
file_write(hyper_params['log_file'], "\nModel Built!\nStarting Training...\n")

if hyper_params['data_parallel']: model = nn.DataParallel(model, dim=0).cuda()

best_val_loss = None
best_val_acc = None

try:
    for epoch in range(1, hyper_params['epochs'] + 1):
        epoch_start_time = time.time()
        train(train_reader)
        val_loss, val_acc = evaluate(test, negs, history, i_hist)
        
        ss  = '-' * 89
        ss += '\n| end of epoch {:3d} | time: {:5.2f}s | valid loss {:5.4f} | val HR@10 = {:5.4f}'.format(epoch, (time.time() - epoch_start_time), val_loss, val_acc)
        ss += '\n'
        ss += '-' * 89
        file_write(hyper_params['log_file'], ss)
        
        # if not best_val_acc or val_acc >= best_val_acc:
        if not best_val_loss or val_loss < best_val_loss:
            with open(hyper_params['model_file_name'], 'wb') as f: torch.save(model, f)
            best_val_loss = val_loss
            best_val_acc = val_acc

except KeyboardInterrupt: print('Exiting from training early')

with open(hyper_params['model_file_name'], 'rb') as f: model = torch.load(f)
test_loss, test_acc = evaluate(test, negs, history, i_hist)

ss  = '=' * 89
ss += '\n| End of training | test loss {:5.4f} | test HR@10 = {:5.4f}'.format(test_loss, test_acc)
ss += '\n'
ss += '=' * 89
file_write(hyper_params['log_file'], ss)