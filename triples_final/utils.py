import pickle
import json

import torch
from hyper_params import *

from torch.nn.utils.rnn import pack_padded_sequence, pad_packed_sequence

LongTensor = torch.LongTensor
FloatTensor = torch.FloatTensor

is_cuda_available = torch.cuda.is_available() and hyper_params['use_cuda'] == True

if is_cuda_available: 
    print("Using CUDA...\n")
    LongTensor = torch.cuda.LongTensor
    FloatTensor = torch.cuda.FloatTensor

def before_rnn(seq_tensor, seq_lengths):
    # print(seq_lengths)
    seq_lengths = LongTensor(seq_lengths)
    seq_lengths, perm_idx = seq_lengths.sort(0, descending=True)
    _, reverse_perm_idx = perm_idx.sort(0)
    seq_tensor = seq_tensor[perm_idx]

    return pack_padded_sequence(seq_tensor, seq_lengths.cpu().numpy(), batch_first=True), reverse_perm_idx

def after_rnn(output, reverse_perm_idx):
    ret, _ = pad_packed_sequence(output, batch_first=True)
    return ret[reverse_perm_idx]

def save_obj(obj, name):
    with open(name + '.pkl', 'wb') as f:
        pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)

def save_obj_json(obj, name):
    with open(name + '.json', 'w') as f:
        json.dump(obj, f)

def load_obj(name):
    with open(name + '.pkl', 'rb') as f:
        return pickle.load(f)

def load_obj_json(name):
    with open(name + '.json', 'r') as f:
        return json.load(f)

def file_write(log_file, s):
    print(s)
    f = open(log_file, 'a')
    f.write(s+'\n')
    f.close()

def clear_log_file(log_file):
    f = open(log_file, 'w')
    f.write('')
    f.close()

def pretty_print(h):
    print("{")
    for key in h:
        print(' ' * 4 + str(key) + ': ' + h[key])
    print('}\n')