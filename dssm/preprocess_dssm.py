import random
import os
import sys
reload(sys)
sys.setdefaultencoding("utf8")

from hyper_params_dssm import *
from utils import *

history = {}
train = {}
test = {}

f = open(hyper_params['data_base'] + 'ratings.dat')
lines = f.readlines()

max_time, min_time = 0, 1000000000000000000

for line in lines:
	temp = map(int, line.strip().split("::"))

	max_time = max(max_time, int(temp[3]))
	min_time = min(min_time, int(temp[3]))

user_hist = {}
item_hist = {}
user_hist_items = {}

for line in lines:
	temp = map(int, line.strip().split("::"))

	if temp[0] not in user_hist: user_hist[temp[0]] = []
	if temp[0] not in user_hist_items: user_hist_items[temp[0]] = set()
	user_hist[temp[0]].append([temp[1], temp[2], float(temp[3] - min_time) / float(max_time - min_time)])
	user_hist_items[temp[0]].add(temp[1])

	if temp[1] not in item_hist: item_hist[temp[1]] = []
	item_hist[temp[1]].append([temp[0], temp[2], float(temp[3] - min_time) / float(max_time - min_time)])

for user in user_hist:
	user_hist[user].sort(key=lambda x: x[2])

for item in item_hist:
	item_hist[item].sort(key=lambda x: x[2])

for user in user_hist:
	if len(user_hist[user]) >= hyper_params['user_hist_size'] + 1 + 1:
		history[user] = [ i[0] for i in user_hist[user][:int(hyper_params['user_hist_size'])]]
		train[user] = [ i[0] for i in user_hist[user][int(hyper_params['user_hist_size']):-1]]
		test[user] = [ user_hist[user][-1][0] ]

negs = {}
all_items = item_hist.keys()

for user in user_hist:
	negs[user] = set()

	while len(negs[user]) < hyper_params['no_negs']:
		item = all_items[random.randint(0, len(all_items) - 1)]
		if item not in user_hist_items[user]:
			negs[user].add(item)

for user in negs:
	negs[user] = list(negs[user])

f = open(hyper_params['data_base'] + 'movies.dat')
lines = f.readlines()
max_item_no = 0

for line in lines:
	temp = line.strip().split("::")
	max_item_no = max(max_item_no, int(temp[0]))

for i in range(max_item_no):
	if i not in item_hist: item_hist[i] = []

save_obj_json(train, hyper_params['data_base'] + "train_dssm")
save_obj_json(test, hyper_params['data_base'] + "test_dssm")
save_obj_json(negs, hyper_params['data_base'] + "negs_dssm")
save_obj_json(history, hyper_params['data_base'] + "history_dssm")
save_obj_json(item_hist, hyper_params['data_base'] + "item_hist_dssm")
save_obj_json(user_hist, hyper_params['data_base'] + "user_hist_dssm")
