import os
import sys
import random
reload(sys)
sys.setdefaultencoding("utf8")

from hyper_params import *
from utils import *





# Calculating min and max time for scaling later
f = open(hyper_params['data_base'] + 'ratings.dat')
lines = f.readlines()

max_time, min_time = 0, 1000000000000000000

for line in lines:
	temp = map(int, line.strip().split("::"))

	max_time = max(max_time, int(temp[3]))
	min_time = min(min_time, int(temp[3]))





# Calculating user and item histories
user_hist_old = {}
item_hist_old = {}

for line in lines:
	temp = map(int, line.strip().split("::"))

	if temp[0] not in user_hist_old: user_hist_old[temp[0]] = []
	if temp[1] not in item_hist_old: item_hist_old[temp[1]] = []
	
	user_hist_old[temp[0]].append([temp[1], temp[2], float(temp[3] - min_time) / float(max_time - min_time)])
	item_hist_old[temp[1]].append([temp[0], temp[2], float(temp[3] - min_time) / float(max_time - min_time)])





# Sorting user and item histories
for user in user_hist_old:
	user_hist_old[user].sort(key=lambda x: x[2])

for item in item_hist_old:
	item_hist_old[item].sort(key=lambda x: x[2])





# Number items according to train
item_map = {}
user_map = {}
count_now = 1 # Start from 1 because padding
count_user = 1 # Start from 1 because padding

for user in user_hist_old:

	if len(user_hist_old[user]) > hyper_params['max_user_hist']: continue
	if len(user_hist_old[user]) < hyper_params['min_user_hist']: continue

	if user not in user_map:
		user_map[user] = count_user
		count_user += 1

	for item in user_hist_old[user][:-1]: # Train split NOT CONSIDERED
		if item[0] not in item_map:
			item_map[item[0]] = count_now
			count_now += 1





# Numbering histories
user_hist = {}
item_hist = {}

for item in item_hist_old:
	if item in item_map:
		item_hist[item_map[item]] = []
		for user in item_hist_old[item]:
			if user[0] in user_map:
				item_hist[item_map[item]].append([ user_map[user[0]], user[1], user[2] ])

for user in user_hist_old:
	if user in user_map:
		user_hist[user_map[user]] = []
		for item in user_hist_old[user]:
			if item[0] in item_map:
				user_hist[user_map[user]].append([ item_map[item[0]], item[1], item[2] ])

assert len(user_hist) == count_user - 1
assert len(item_hist) == count_now - 1





# Calculating all items for each user
user_hist_items = {}
for user in user_hist:

	if len(user_hist[user]) > hyper_params['max_user_hist']: continue
	if len(user_hist[user]) < hyper_params['min_user_hist']: continue
	
	user_hist_items[user] = set()

	for item in user_hist[user]:
		if item[0] not in user_hist_items[user]:
			user_hist_items[user].add(item[0])





# Calculating the train and test splits, along with randomly sampled negative examples
negs = {}
train = {}
test = {}

users_kept = 0
all_items = list(item_hist.keys())

for user in user_hist:

	if len(user_hist[user]) > hyper_params['max_user_hist']: continue
	if len(user_hist[user]) < hyper_params['min_user_hist']: continue
	
	users_kept += 1

	negs[user] = set()
	while len(negs[user]) < hyper_params['no_negs']:
		item = all_items[random.randint(0, len(all_items) - 1)]
		if item not in user_hist_items[user]:
			negs[user].add(item)
	negs[user] = list(negs[user])

	train_split = user_hist[user][:-1]
	test_split = user_hist[user][-1]

	train[user] = train_split
	test[user] = test_split

print("users_kept = " + str(users_kept))
assert users_kept == len(user_hist)





# Saving all objects
save_obj_json(train, hyper_params['data_base'] + "train_triples")
save_obj_json(test, hyper_params['data_base'] + "test_triples")
save_obj_json(item_hist, hyper_params['data_base'] + "item_hist_triples")
save_obj_json(user_hist, hyper_params['data_base'] + "user_hist_triples")
save_obj_json(negs, hyper_params['data_base'] + "negs_triples")
