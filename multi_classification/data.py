import numpy as np
from tqdm import tqdm
import torch
from torch.autograd import Variable

from utils import *

'''
Main function called in the main.py file

hyper_params: object having all hyper parameters

returns: object of class DataReader for train data ; test array ; number of users ; number of items
'''
def load_data(hyper_params):
    
    file_write(hyper_params['log_file'], "Started reading data file")
    
    train = load_obj_json(hyper_params['data_base'] + 'train_classification')
    test = load_obj_json(hyper_params['data_base'] + 'test_classification')
    user_hist = load_obj_json(hyper_params['data_base'] + 'user_hist_classification')
    item_hist = load_obj_json(hyper_params['data_base'] + 'item_hist_classification')
    
    file_write(hyper_params['log_file'], "Data Files loaded!")

    train_reader = DataReader(hyper_params, train, len(user_hist), item_hist)
    
    x_test, y_test = [], []
    x_batch = []
    y_batch = []

    for user in test:

        temp = []
        for j in range(len(train[user]) - hyper_params['window_size'], len(train[user])):
            temp.append(train[user][j][0])

        x_batch.append(temp)
        y_batch.append(int(test[user][0]))

        if len(y_batch) == hyper_params['batch_size']:
            x_test.append(x_batch)
            y_test.append(y_batch)
            
            x_batch = []
            y_batch = []

    return train_reader, x_test, y_test, len(user_hist), len(item_hist)

'''
Class which manages train data
Has a function iter which yields the x_batch, y_batch

hyper_params: object having all hyper parameters
data        : training data
num_users   : total number of users
item_hist   : object having all item history (containing users)
'''
class DataReader:

    def __init__(self, hyper_params, data, num_users, item_hist):
        self.hyper_params = hyper_params
        self.batch_size = hyper_params['batch_size']
        self.item_hist = item_hist
        self.num_users = num_users
        self.num_items = len(item_hist)
        self.data = data

        self.number()

    '''
    Counts the number of batches
    '''
    def number(self):
        count = 0

        for user in self.data:
            y_batch = []

            for i in range(self.hyper_params['window_size'], len(self.data[user]) - 1):

                y_batch.append(0)

                if len(y_batch) == self.batch_size:
                    y_batch = []
                    count += 1

        self.num_b = count

    '''
    Yields x_batch and y_batch at each time step

    returns: [
        x_batch_u_hist, (has items)
        x_batch_u_hist_rating, (ratings given by user to above items)
        x_batch_u_hist_time, (when did user give ratings)
        
        x_batch_i_hist, (has users)
        x_batch_i_hist_rating, (rating given to item by above users)
        x_batch_i_hist_time, (when was rating given)
        
        x_batch_user, (users)
        x_batch_item (items)
    ], y_batch (rating given by x_batch_user to x_batch_item)
    '''
    def iter(self):
        for user in self.data:

            x_batch = []
            y_batch = []

            for i in range(self.hyper_params['window_size'], len(self.data[user]) - 1):

                temp = []
                for j in range(i - self.hyper_params['window_size'], i):
                    temp.append(self.data[user][j][0])

                x_batch.append(temp)
                y_batch.append(int(self.data[user][i+1][0]))

                if len(y_batch) == self.batch_size:

                    yield Variable(torch.cuda.LongTensor(x_batch)), Variable(torch.cuda.LongTensor(y_batch))
                    
                    x_batch = []
                    y_batch = []
