import math
import torch
import random
import numpy as np
from torch import nn
from torch.autograd import Variable
import torch.nn.functional as F

from utils import *

class Attention(nn.Module):
    def __init__(self, in_size):
        super(Attention, self).__init__()
        # self.attention_weights = nn.Parameter(torch.FloatTensor(in_size), requires_grad = True)
        self.attention_dense_layer = nn.Linear(in_size, 1)
        self.attention_dropout = nn.Dropout(0.2)
        # self.attention_activation = nn.Tanh()
        self.attention_activation = nn.ReLU()
        self.softmax = nn.Softmax(1)

    def forward(self, hidden_states):
        shape = hidden_states.shape
        scores = self.attention_dropout(self.attention_activation(self.attention_dense_layer(hidden_states.contiguous().view(-1, shape[-1])).view(-1, shape[1])))
        scores = self.softmax(scores)
        # print(scores)
        # print(torch.sum(scores, 1))
        return scores.unsqueeze(1).bmm(hidden_states).squeeze(1)
        # print(final)
        # print("\n\n")
    
    def forward_parameter(self, hidden_states):
        scores = self.attention_activation(hidden_states.matmul(self.attention_weights))
        scores = self.softmax(scores)
        # print(scores)
        # print(torch.sum(scores, 1))
        weighted = torch.mul(hidden_states, scores.unsqueeze(-1).expand_as(hidden_states))
        # print(weighted.sum(1).squeeze())
        # print("\n\n")
        return weighted.sum(1).squeeze()

class Model(nn.Module):
    def __init__(self, hyper_params):
        super(Model, self).__init__()
        self.hyper_params = hyper_params

        self.user_embed = nn.Embedding(hyper_params['total_users'] + 1, hyper_params['user_embed_size'])
        self.item_embed = nn.Embedding(hyper_params['total_items'] + 1, hyper_params['item_embed_size'])

        self.user_rnn = nn.GRU(hyper_params['item_embed_size'], hyper_params['user_rnn_size'], hyper_params['user_rnn_layers'], dropout=hyper_params['user_rnn_dropout'], bidirectional=hyper_params['user_rnn_bi'], batch_first=True)
        self.item_rnn = nn.GRU(hyper_params['user_embed_size'], hyper_params['item_rnn_size'], hyper_params['item_rnn_layers'], dropout=hyper_params['item_rnn_dropout'], bidirectional=hyper_params['item_rnn_bi'], batch_first=True)
        
        if hyper_params['user_attention']:
            self.user_attention = Attention(hyper_params['user_rnn_size'])

        if hyper_params['item_attention']:
            self.item_attention = Attention(hyper_params['item_rnn_size'])

        if hyper_params['activation_last'] == 'sigmoid':
            self.activation_last = nn.Sigmoid()
        elif hyper_params['activation_last'] == 'tanh':
            self.activation_last = nn.Tanh()
        elif hyper_params['activation_last'] == 'relu':
            self.activation_last = nn.ReLU()

        self.dropout = nn.Dropout(hyper_params['dropout'])

        self.batch_norm_user_embedding = nn.BatchNorm1d(hyper_params['user_embed_size'])
        self.batch_norm_item_embedding = nn.BatchNorm1d(hyper_params['item_embed_size'])
        self.batch_norm_user_rnn = nn.BatchNorm1d(hyper_params['user_rnn_size'])
        self.batch_norm_item_rnn = nn.BatchNorm1d(hyper_params['item_rnn_size'])

        if self.hyper_params['merge_type'] == 'concat': 
            prev = hyper_params['user_rnn_size'] + hyper_params['user_embed_size'] + hyper_params['item_rnn_size'] + hyper_params['item_embed_size']
            self.layer1 = nn.Linear(prev, 1)

    def forward(self, src):

        user = self.user_embed(src[0])               # [bsz x user_embed_size]
        user_hist = self.item_embed(src[1][0])          # [bsz x history_len x user_embed_size]        
        item = self.item_embed(src[2])               # [bsz x item_embed_size]
        item_hist = self.user_embed(src[3][0])          # [bsz x history_len x item_embed_size]

        if self.hyper_params['embed_dropout'] == True: 
            user = self.dropout(user)
            user_hist = self.dropout(user_hist)
            item = self.dropout(item)
            item_hist = self.dropout(item_hist)
        
        self.user_rnn_hidden = self.init_hidden(user_hist.size(0), self.hyper_params['user_rnn_bi'], self.hyper_params['user_rnn_layers'], self.hyper_params['user_rnn_size'])
        self.user_rnn.flatten_parameters()
        if self.hyper_params['dynamic_rnn'] == True:
            user_hist, reverse_perm = before_rnn(user_hist, src[1][1])
        user_rnn_output, self.user_rnn_hidden = self.user_rnn(user_hist, self.user_rnn_hidden) # [bsz x user_rnn_size]
        if self.hyper_params['dynamic_rnn'] == True:
            user_rnn_output = after_rnn(user_rnn_output, reverse_perm)

        self.item_rnn_hidden = self.init_hidden(item_hist.size(0), self.hyper_params['item_rnn_bi'], self.hyper_params['item_rnn_layers'], self.hyper_params['item_rnn_size'])
        self.item_rnn.flatten_parameters()
        if self.hyper_params['dynamic_rnn'] == True:
            item_hist, reverse_perm = before_rnn(item_hist, src[3][1])
        item_rnn_output, self.item_rnn_hidden = self.item_rnn(item_hist, self.item_rnn_hidden) # [bsz x item_rnn_size]
        if self.hyper_params['dynamic_rnn'] == True:
            item_rnn_output = after_rnn(item_rnn_output, reverse_perm)

        if self.hyper_params['user_attention'] == True: user_attention_output = self.user_attention(user_rnn_output)
        else: user_attention_output = user_rnn_output[:, -1, :]

        if self.hyper_params['item_attention'] == True: item_attention_output = self.item_attention(item_rnn_output)
        else: item_attention_output = item_rnn_output[:, -1, :]

        user_attention_output = user_attention_output.contiguous()
        item_attention_output = item_attention_output.contiguous()

        user = self.batch_norm_user_embedding(user)
        item = self.batch_norm_item_embedding(item)
        user_attention_output = self.batch_norm_user_rnn(user_attention_output)
        item_attention_output = self.batch_norm_item_rnn(item_attention_output)

        output_user = torch.cat([user_attention_output, user], dim = -1)
        output_item = torch.cat([item_attention_output, item], dim = -1)

        if self.hyper_params['merge_type'] == 'concat':
            output = torch.cat([output_user, output_item], dim = -1)
            output = self.layer1(output)
            output = self.dropout(output)
        elif self.hyper_params['merge_type'] == 'dot':
            output =  torch.mm(output_user, output_item.transpose(0, 1)).diag().unsqueeze(-1)
        # Do I need a batch norm now ?
         
        output = self.activation_last(output)

        # if self.hyper_params['activation_last'] == 'tanh':
        #     output = output + 1.0
        #     output = output / 2.0

        return output # [bsz x last_layer_dim]

    def init_hidden(self, batch_size, bi, layers, size):
        mul = 1
        if bi == True: mul = 2
        # return Variable(torch.zeros(mul * layers, batch_size, size).cuda(), requires_grad=True)
        return Variable(torch.randn(mul * layers, batch_size, size).cuda(), requires_grad=True)

    def repack(self, h):
        if type(h) == Variable:
            return Variable(h.data.cuda(), requires_grad=True)
        else:
            return tuple(repackage_hidden(v) for v in h)
