import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable

import numpy as np
import time
import random
import pickle
import json
import gc
import datetime as dt
from tqdm import tqdm

from data import load_data
from model import Model
from utils import *
from hyper_params import *

def evaluate(reader):
    model.eval()

    metrics = {}
    metrics['CP'] = 0.0
    metrics['NCP'] = 0.0
    metrics['ZEROS'] = 0.0
    metrics['loss'] = 0.0

    zeros_max = Variable(torch.zeros(int(hyper_params['batch_size'] / 2)).float().cuda())

    correct = 0
    not_correct = 0
    zeros = 0
    total = 0
    batch = 0

    for x, y in reader.iter():
        batch += 1
        if batch > hyper_params['testing_batch_limit']: break

        output = model(x)

        metrics['loss'] += criterion(output, y).data

        output = output[:,0]

        temp_correct = int(torch.sum(torch.gt(y * output, 0)))
        temp_not_correct = int(torch.sum(torch.lt(y * output, 0)))
        temp_zeros = int(torch.sum(torch.eq(y * output, 0)))

        correct += temp_correct
        not_correct += temp_not_correct
        zeros += temp_zeros

        assert temp_correct + temp_not_correct + temp_zeros == int(y.shape[0])

        total += int(y.shape[0])

    assert correct + not_correct + zeros == total

    metrics['CP'] = float(correct) / float(total)
    metrics['CP'] *= 100.0
    metrics['CP'] = round(metrics['CP'], 4)

    metrics['NCP'] = float(not_correct) / float(total)
    metrics['NCP'] *= 100.0
    metrics['NCP'] = round(metrics['NCP'], 4)

    metrics['ZEROS'] = float(zeros) / float(total)
    metrics['ZEROS'] *= 100.0
    metrics['ZEROS'] = round(metrics['ZEROS'], 4)

    metrics['loss'] = float(metrics['loss'][0]) / float(batch)
    metrics['loss'] = round(metrics['loss'], 4)

    return metrics

def train(reader):
    model.train()
    total_loss = 0
    start_time = time.time()
    batch = 0

    for x, y in reader.iter():
        batch += 1
        if batch > hyper_params['training_batch_limit']: return
        
        model.zero_grad()
        optimizer.zero_grad()

        output = model(x)

        # print(y)
        # print(output)
        # print("\n")
        
        loss = criterion(output, y)
        loss.backward()

        torch.nn.utils.clip_grad_norm(model.parameters(), hyper_params['exploding_clip'])
        optimizer.step()

        total_loss += loss.data

        if batch % hyper_params['batch_log_interval'] == 0 and batch > 0:
            cur_loss = (total_loss[0] / hyper_params['batch_log_interval'])
            elapsed = time.time() - start_time

            ss = '| epoch {:3d} | {:5d}/{:5d} batches | ms/batch {:5.2f} | loss {:5.4f}'.format(
                epoch, batch, int(min(train_reader.num_b, hyper_params['training_batch_limit'])),
                elapsed * 1000 / hyper_params['batch_log_interval'], cur_loss)
            
            file_write(hyper_params['log_file'], ss)

            total_loss = 0
            start_time = time.time()

train_reader, test_reader, total_users, total_items = load_data(hyper_params)
hyper_params['total_users'] = total_users
hyper_params['total_items'] = total_items

file_write(hyper_params['log_file'], "\n\nSimulation run on: " + str(dt.datetime.now()) + "\n\n")
file_write(hyper_params['log_file'], "Data reading complete!")
file_write(hyper_params['log_file'], "Number of train batches: {:4d}".format(train_reader.num_b))
file_write(hyper_params['log_file'], "Number of test batches: {:4d}".format(test_reader.num_b))
file_write(hyper_params['log_file'], "Total Users: " + str(total_users))
file_write(hyper_params['log_file'], "Total Items: " + str(total_items) + "\n")

model = Model(hyper_params)
model.cuda()

class HingeLoss(torch.nn.Module):
    def __init__(self):
        super(HingeLoss,self).__init__()

    def forward(self, output, y):
        output = output[:, 0]
        summed  = -1 * (y * output)
        
        # is_same_output = torch.lt(output, 0.0001).float()
        # summed += 100.0 * is_same_output
        
        return torch.mean(summed)

criterion = HingeLoss()
# criterion = nn.MSELoss()
# criterion = nn.CrossEntropyLoss(size_average=True)

if hyper_params['optimizer'] == 'adagrad':
    optimizer = torch.optim.Adagrad(model.parameters(), lr=hyper_params['learning_rate'])
elif hyper_params['optimizer'] == 'adadelta':
    optimizer = torch.optim.Adadelta(model.parameters())

file_write(hyper_params['log_file'], str(model))
file_write(hyper_params['log_file'], "\nModel Built!\nStarting Training...\n")

if hyper_params['data_parallel']: model = nn.DataParallel(model, dim=0).cuda()

best_val_hr = None

try:
    for epoch in range(1, hyper_params['epochs'] + 1):
        epoch_start_time = time.time()
        train(train_reader)
        
        metrics = evaluate(train_reader)
        
        string = ""
        for m in metrics:
            string += " | " + m + ' = ' + str(metrics[m])
        string += ' (TRAIN)'

        metrics = evaluate(test_reader)
        
        for m in metrics:
            string += " | " + m + ' = ' + str(metrics[m])
        string += ' (TEST)'

        ss  = '-' * 89
        ss += '\n| end of epoch {:3d} | time: {:5.2f}s'.format(epoch, (time.time() - epoch_start_time))
        ss += string
        ss += '\n'
        ss += '-' * 89
        file_write(hyper_params['log_file'], ss)
        
        if not best_val_hr or metrics['CP'] >= best_val_hr:
            with open(hyper_params['model_file_name'], 'wb') as f: torch.save(model, f)
            best_val_hr = metrics['CP']

except KeyboardInterrupt: print('Exiting from training early')

with open(hyper_params['model_file_name'], 'rb') as f: model = torch.load(f)
metrics = evaluate(test_reader)

string = ""
for m in metrics:
    string += " | " + m + ' = ' + str(metrics[m])

ss  = '=' * 89
ss += '\n| End of training'
ss += string
ss += '\n'
ss += '=' * 89
file_write(hyper_params['log_file'], ss)