import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable

import numpy as np
import time
import pickle
import json
import gc
import datetime as dt
from tqdm import tqdm

from data import load_data
from model import Model
from utils import *
from hyper_params import *

def evaluate(reader):
    model.eval()

    metrics = {}
    metrics['HR@1'] = 0.0
    metrics['HR@5'] = 0.0
    metrics['HR@10'] = 0.0
    metrics['HR@50'] = 0.0
    metrics['HR@0'] = 0.0

    total = 0.0

    for x, y in reader.iter_eval():

        output = model(x)

        for i in range(hyper_params['batch_size']):
            if hyper_params['bptt'] == True:
                _, top = torch.sort(output[i, -1, :], descending=True)
            else:
                _, top = torch.sort(output[i, :], descending=True)

            for k in [1, 5, 10, 50]:
                string = 'HR@' + str(k)
                tt = top[:k].data
                
                # print(tt)
                # print(y)

                # for ii in y[i]:
                #     if int(ii) in tt:
                #         metrics[string] += 1.0
                
                if int(y[i][0]) in tt:
                    metrics[string] += 1.0
            
            if 0 in top[:10].data:
                metrics['HR@0'] += 1.0

            total += 1.0

    for k in [0, 1, 5, 10, 50]:
        string = 'HR@' + str(k)
        metrics[string] /= total
        metrics[string] *= 100.0
        metrics[string] = round(metrics[string], 4)

    return metrics

def train(reader):
    model.train()
    total_loss = 0
    start_time = time.time()
    batch = 0

    for x, y in reader.iter():
        batch += 1
        if batch > hyper_params['training_batch_limit']: return
        
        # model.zero_grad()
        optimizer.zero_grad()

        output = model(x)

        # print(y)
        # print(output)
        # print("\n")
        
        # if hyper_params['bptt'] == True: loss = criterion(output.view(-1, hyper_params['total_items'] + 1), y.view(-1))
        # else: loss = criterion(output, y)

        # if hyper_params['augmented_loss'] == True:

        # if epoch % 2 == 0:
        if epoch >= 3:
            # embed_normalized = F.normalize(model.embed.weight.transpose(0, 1), p=2, dim=-1)
            embed_normalized = model.embed.weight.transpose(0, 1)
            y_true_kld = torch.mm(model.embed(y).squeeze(0), embed_normalized)
            y_true_kld = Variable(F.softmax(y_true_kld, -1).data, requires_grad=False)
        else:
            y_true_kld = torch.cuda.FloatTensor(y.shape[1], hyper_params['total_items'] + 1).zero_()
            y_true_kld.scatter_(1, y.transpose(0, 1).data, 1)
            y_true_kld = Variable(y_true_kld, requires_grad=False)

        y_pred_kld = F.log_softmax(output.view(-1, hyper_params['total_items'] + 1), -1)

        # print("pred:")
        # print(torch.sum(y_pred_kld, 1))
        # print("true:")
        # print(torch.sum(y_true_kld, 1))
        
        # if epoch >= 2:
        #     print(torch.sum(y_true_kld, -1))
        #     print(y_pred_kld)
        #     print('\n')

        loss_kld = criterion_kld(y_pred_kld, y_true_kld)
        # loss_kld.backward()

        loss = (hyper_params['gamma'] * loss_kld)

        loss.backward()

        torch.nn.utils.clip_grad_norm(model.parameters(), hyper_params['exploding_clip'])
        optimizer.step()

        total_loss += loss.data

        if batch % hyper_params['batch_log_interval'] == 0 and batch > 0:
            cur_loss = (total_loss[0] / hyper_params['batch_log_interval'])
            elapsed = time.time() - start_time

            ss = '| epoch {:3d} | {:5d}/{:5d} batches | ms/batch {:5.2f} | loss {:5.4f}'.format(
                epoch, batch, int(min(train_reader.num_b, hyper_params['training_batch_limit'])),
                elapsed * 1000 / hyper_params['batch_log_interval'], cur_loss)
            
            file_write(hyper_params['log_file'], ss)

            total_loss = 0
            start_time = time.time()

train_reader, test_reader, val_reader, total_users, total_items = load_data(hyper_params)
hyper_params['total_users'] = total_users
hyper_params['total_items'] = total_items

file_write(hyper_params['log_file'], "\n\nSimulation run on: " + str(dt.datetime.now()) + "\n\n")
file_write(hyper_params['log_file'], "Data reading complete!")
file_write(hyper_params['log_file'], "Number of train batches: {:4d}".format(train_reader.num_b))
file_write(hyper_params['log_file'], "Total Users: " + str(total_users))
file_write(hyper_params['log_file'], "Total Items: " + str(total_items) + "\n")

model = Model(hyper_params)
model.cuda()
model.rnn_hidden = model.init_hidden(model.hyper_params['batch_size'], model.hyper_params['rnn_bi'], model.hyper_params['rnn_layers'], model.hyper_params['rnn_size'])

# criterion = nn.CrossEntropyLoss(size_average=True)
criterion_kld = nn.KLDivLoss(size_average=True)
soft = nn.Softmax(1)

optimizer = torch.optim.Adagrad(model.parameters(), lr=hyper_params['learning_rate'])
# optimizer = torch.optim.Adadelta(model.parameters())

file_write(hyper_params['log_file'], str(model))
file_write(hyper_params['log_file'], "\nModel Built!\nStarting Training...\n")

if hyper_params['data_parallel']: model = nn.DataParallel(model, dim=0).cuda()

best_val_hr = None

try:
    for epoch in range(1, hyper_params['epochs'] + 1):
        epoch_start_time = time.time()
        train(train_reader)
        metrics = evaluate(val_reader)
        
        string = ""
        for m in metrics:
            string += " | " + m + ' = ' + str(metrics[m])

        ss  = '-' * 89
        ss += '\n| end of epoch {:3d} | time: {:5.2f}s'.format(epoch, (time.time() - epoch_start_time))
        ss += string
        ss += '\n'
        ss += '-' * 89
        file_write(hyper_params['log_file'], ss)
        
        if not best_val_hr or metrics['HR@10'] >= best_val_hr:
            with open(hyper_params['model_file_name'], 'wb') as f: torch.save(model, f)
            best_val_hr = metrics['HR@10']

except KeyboardInterrupt: print('Exiting from training early')

with open(hyper_params['model_file_name'], 'rb') as f: model = torch.load(f)
metrics = evaluate(test_reader)

string = ""
for m in metrics:
    string += " | " + m + ' = ' + str(metrics[m])

ss  = '=' * 89
ss += '\n| End of training'
ss += string
ss += '\n'
ss += '=' * 89
file_write(hyper_params['log_file'], ss)