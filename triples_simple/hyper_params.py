hyper_params = {
    'data_base': '../saved_data/',
    'model_file_name': '',
    'log_file': '',
    'data_split': [0.8, 0.2], # Train : Test
    'max_user_hist': 100,
    'min_user_hist': 5,
    
    'bad_set_length': 50,
    'min_thresh_for_good': float(4),
    'max_thresh_for_bad': float(2.5),
    'min_count_for_bad': 25,

    'learning_rate': 0.1, # if optimizer is adadelta, learning rate is not required
    'optimizer': 'adagrad',
    'loss_type': 'exp',
    'm_loss': float(1),
    'weight_decay': float(1e-6),

    'epochs': 240,
    'batch_size': 128, # Needs to be even

    'user_embed_size': 16,
    'item_embed_size': 16,
    'embed_dropout': True,
    'activation_last': 'tanh',
    'dropout': 0.2,

    'exploding_clip': 0.25,
    'training_batch_limit': 10000000,
    'training_random_keeping_chance': 1.0,
    'number_users_to_keep': 50000000,
    'user_limit_inversion': 200,
    'testing_batch_limit': 3813,
    'batch_log_interval': 10000,

    'use_cuda': True, # Only used if cuda is available, otherwise cuda is not used
    'data_parallel': False,
}

file_name = '_optimizer_' + str(hyper_params['optimizer'])
if hyper_params['optimizer'] != 'adadelta':
    file_name += '_lr_' + str(hyper_params['learning_rate'])
file_name += '_user_embed_size_' + str(hyper_params['user_embed_size'])
file_name += '_item_embed_size_' + str(hyper_params['item_embed_size'])
file_name += '_weight_decay_' + str(hyper_params['weight_decay'])
file_name += '_activation_last_' + str(hyper_params['activation_last'])
file_name += '_embed_dropout_' + str(hyper_params['embed_dropout'])
file_name += '_dropout_' + str(hyper_params['dropout'])

hyper_params['log_file'] = '../saved_logs/triples_simple_log' + file_name + '.txt'
hyper_params['model_file_name'] = '../saved_models/triples_simple_model' + file_name + '.pt'