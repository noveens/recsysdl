import torch
import numpy as np
from torch.autograd import Variable

from utils import *

class HingeLoss(torch.nn.Module):
    def __init__(self, batch_size, loss_type, m_loss):
        super(HingeLoss,self).__init__()

        self.loss_type = loss_type
        self.m_loss = m_loss

        self.first_indices = LongTensor(range(0, batch_size, 2))
        self.second_indices = LongTensor(range(1, batch_size, 2))
        self.zeros_while_max = torch.zeros(int(batch_size / 2)).float()
        if is_cuda_available: self.zeros_while_max = self.zeros_while_max.cuda()
        self.zeros_while_max = Variable(self.zeros_while_max)
        self.exp = Variable(FloatTensor([np.e]))

    def forward(self, output, y):
        # print(output.shape)
        # exit(0)
        # print(y.shape)
        output_first = output[self.first_indices]
        output_second = output[self.second_indices]

        y_first = y[self.first_indices]
        y_second = y[self.second_indices]

        left = torch.gt(y_first, y_second).float() - torch.lt(y_first, y_second).float()
        
        ############ Reference: https://papers.nips.cc/paper/3708-ranking-measures-and-loss-functions-in-learning-to-rank.pdf
        if self.loss_type == 'hinge':
            summed = self.m_loss + (left * (output_second - output_first))
        
        elif self.loss_type == 'exp':
            summed = torch.pow(self.exp, left * (output_second - output_first))

        elif self.loss_type == 'logistic':
            summed = torch.log(self.m_loss + torch.pow(self.exp, left * (output_second - output_first)))
        
        # summed += torch.eq(output_first, output_second).float() * 100.0

        summed = torch.max(self.zeros_while_max, summed)
        return torch.mean(summed)
