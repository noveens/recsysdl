import pickle
import json
from torch.nn.utils.rnn import pack_padded_sequence, pad_packed_sequence

def save_obj(obj, name):
    with open(name + '.pkl', 'wb') as f:
        pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)

def save_obj_json(obj, name):
    with open(name + '.json', 'w') as f:
        json.dump(obj, f)

def load_obj(name):
    with open(name + '.pkl', 'rb') as f:
        return pickle.load(f)

def load_obj_json(name):
    with open(name + '.json', 'r') as f:
        return json.load(f)

def file_write(log_file, s):
    print(s)
    f = open(log_file, 'a')
    f.write(s+'\n')
    f.close()

def clear_log_file(log_file):
    f = open(log_file, 'w')
    f.write('')
    f.close()

def pretty_print(h):
    print("{")
    for key in h:
        print(' ' * 4 + str(key) + ': ' + h[key])
    print('}\n')

def before_rnn(seq_tensor, seq_lengths):
    print(seq_lengths)
    return pack_padded_sequence(seq_tensor, seq_lengths.cpu().numpy(), batch_first=True)

def after_rnn(output):
    ret, _ = pad_padded_sequence(output, batch_first=True)
    return ret