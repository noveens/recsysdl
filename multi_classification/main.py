import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable

import numpy as np
import time
import pickle
import json
import gc
import datetime as dt
from tqdm import tqdm

from data import load_data
from model import Model
from utils import *
from hyper_params import *

def evaluate(x_test, y_test):
    model.eval()

    metrics = {}
    metrics['HR@1'] = 0.0
    metrics['HR@5'] = 0.0
    metrics['HR@10'] = 0.0
    metrics['HR@50'] = 0.0

    total = 0.0

    for i in tqdm(range(min(len(y_test), hyper_params['testing_batch_limit']))):
        x, y = x_test[i], y_test[i]

        output = model(Variable(torch.cuda.LongTensor(x)))

        for i in range(hyper_params['batch_size']):
            # print(output.shape)
            _, top = torch.sort(output[i, :], descending=True)

            for k in [1, 5, 10, 50]:
                string = 'HR@' + str(k)
                tt = top[:k].data
                # print(tt)
                # print(y)
                if int(y[i]) in tt:
                    metrics[string] += 1.0
                total += 1.0

    for k in [1, 5, 10, 50]:
        string = 'HR@' + str(k)
        metrics[string] /= total

    return metrics

def train(reader):
    model.train()
    total_loss = 0
    start_time = time.time()
    batch = 0

    for x, y in reader.iter():
        batch += 1
        if batch > hyper_params['training_batch_limit']: return
        
        optimizer.zero_grad()

        output = model(x)

        # print(y)
        # print(output)
        # print("\n")
        
        loss = criterion(output, y)
        loss.backward()

        torch.nn.utils.clip_grad_norm(model.parameters(), hyper_params['exploding_clip'])
        optimizer.step()

        total_loss += loss.data

        if batch % hyper_params['batch_log_interval'] == 0 and batch > 0:
            cur_loss = (total_loss[0] / hyper_params['batch_log_interval'])
            elapsed = time.time() - start_time

            ss = '| epoch {:3d} | {:5d}/{:5d} batches | ms/batch {:5.2f} | loss {:5.4f}'.format(
                epoch, batch, int(min(train_reader.num_b, hyper_params['training_batch_limit'])),
                elapsed * 1000 / hyper_params['batch_log_interval'], cur_loss)
            
            file_write(hyper_params['log_file'], ss)

            total_loss = 0
            start_time = time.time()

train_reader, x_test, y_test, total_users, total_items = load_data(hyper_params)
hyper_params['total_users'] = total_users
hyper_params['total_items'] = total_items

file_write(hyper_params['log_file'], "\n\nSimulation run on: " + str(dt.datetime.now()) + "\n\n")
file_write(hyper_params['log_file'], "Data reading complete!")
file_write(hyper_params['log_file'], "Number of train batches: {:4d}".format(train_reader.num_b))
file_write(hyper_params['log_file'], "Total Users: " + str(total_users))
file_write(hyper_params['log_file'], "Total Items: " + str(total_items) + "\n")

model = Model(hyper_params)
model.cuda()
model.item_rnn_hidden = model.init_hidden(hyper_params['batch_size'], model.hyper_params['item_rnn_bi'], model.hyper_params['item_rnn_layers'], model.hyper_params['item_rnn_size'])

criterion = nn.CrossEntropyLoss(size_average=True)

optimizer = torch.optim.Adagrad(model.parameters(), lr=hyper_params['learning_rate'])
# optimizer = torch.optim.Adadelta(model.parameters())

file_write(hyper_params['log_file'], str(model))
file_write(hyper_params['log_file'], "\nModel Built!\nStarting Training...\n")

if hyper_params['data_parallel']: model = nn.DataParallel(model, dim=0).cuda()

best_val_hr = None

try:
    for epoch in range(1, hyper_params['epochs'] + 1):
        epoch_start_time = time.time()
        train(train_reader)
        metrics = evaluate(x_test, y_test)
        
        string = ""
        for m in metrics:
            string += " | " + m + ' = ' + str(metrics[m])

        ss  = '-' * 89
        ss += '\n| end of epoch {:3d} | time: {:5.2f}s'.format(epoch, (time.time() - epoch_start_time))
        ss += string
        ss += '\n'
        ss += '-' * 89
        file_write(hyper_params['log_file'], ss)
        
        if not best_val_hr or metrics['HR@10'] >= best_val_hr:
            with open(hyper_params['model_file_name'], 'wb') as f: torch.save(model, f)
            best_val_hr = metrics['HR@10']

except KeyboardInterrupt: print('Exiting from training early')

with open(hyper_params['model_file_name'], 'rb') as f: model = torch.load(f)
metrics = evaluate(x_test, y_test)

string = ""
for m in metrics:
    string += " | " + m + ' = ' + str(metrics[m])

ss  = '=' * 89
ss += '\n| End of training'
ss += string
ss += '\n'
ss += '=' * 89
file_write(hyper_params['log_file'], ss)