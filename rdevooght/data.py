import numpy as np
from tqdm import tqdm
import torch
from torch.autograd import Variable

from utils import *

'''
Main function called in the main.py file

hyper_params: object having all hyper parameters

returns: object of class DataReader for train data ; test array ; number of users ; number of items
'''
def load_data(hyper_params):
    
    file_write(hyper_params['log_file'], "Started reading data file")
    
    train = load_obj_json(hyper_params['data_base'] + 'train_rdevooght')
    test = load_obj_json(hyper_params['data_base'] + 'test_rdevooght')
    val = load_obj_json(hyper_params['data_base'] + 'val_rdevooght')
    user_hist = load_obj_json(hyper_params['data_base'] + 'user_hist_rdevooght')
    item_hist = load_obj_json(hyper_params['data_base'] + 'item_hist_rdevooght')
    
    file_write(hyper_params['log_file'], "Data Files loaded!")

    train_reader = DataReader(hyper_params, train, len(user_hist), item_hist)
    test_reader = DataReader(hyper_params, test, len(user_hist), item_hist)
    val_reader = DataReader(hyper_params, val, len(user_hist), item_hist)
    
    # x_test, y_test = [], []
    # x_batch = []
    # y_batch = []

    # for user in test:

    #     temp = []
    #     for j in range(len(train[user]) - hyper_params['window_size'], len(train[user])):
    #         temp.append(train[user][j][0])

    #     x_batch.append(temp)
    #     y_batch.append(int(test[user][0]))

    #     if len(y_batch) == hyper_params['batch_size']:
    #         x_test.append(x_batch)
    #         y_test.append(y_batch)
            
    #         x_batch = []
    #         y_batch = []

    return train_reader, test_reader, val_reader, len(user_hist), len(item_hist)

'''
Class which manages train data
Has a function iter which yields the x_batch, y_batch

hyper_params: object having all hyper parameters
data        : training data
num_users   : total number of users
item_hist   : object having all item history (containing users)
'''
class DataReader:

    def __init__(self, hyper_params, data, num_users, item_hist):
        self.hyper_params = hyper_params
        self.batch_size = hyper_params['batch_size']
        self.item_hist = item_hist
        self.num_users = num_users
        self.num_items = len(item_hist)
        self.data = data

        self.number()

    '''
    Counts the number of batches
    '''
    def number(self):
        count = 0

        y_batch = []

        for user in self.data:

            y_batch.append(0)

            if len(y_batch) == self.batch_size:
                
                y_batch = []
                count += 1

        self.num_b = count

    '''
    Yields x_batch and y_batch at each time step

    returns: [
        x_batch_u_hist, (has items)
        x_batch_u_hist_rating, (ratings given by user to above items)
        x_batch_u_hist_time, (when did user give ratings)
        
        x_batch_i_hist, (has users)
        x_batch_i_hist_rating, (rating given to item by above users)
        x_batch_i_hist_time, (when was rating given)
        
        x_batch_user, (users)
        x_batch_item (items)
    ], y_batch (rating given by x_batch_user to x_batch_item)
    '''
    def iter(self):
        x_batch = []
        y_batch = []

        for user in self.data:

            x_batch.append(self.data[user][:-1])

            if self.hyper_params['bptt'] == True: y_batch.append(self.data[user][1:])
            else: y_batch.append(self.data[user][-1])

            if len(y_batch) == self.batch_size:

                yield Variable(torch.cuda.LongTensor(x_batch)), Variable(torch.cuda.LongTensor(y_batch), requires_grad=False)
                
                x_batch = []
                y_batch = []

    def pack_output(self, x_batch, y_batch):
        x_seq_lengths = torch.cuda.LongTensor(map(len, x_batch))

        seq_tensor = Variable(torch.zeros((len(x_batch), x_seq_lengths.max())).long().cuda())
        for idx, (seq, seqlen) in enumerate(zip(x_batch, x_seq_lengths)):
            seq_tensor[idx, :seqlen] = torch.LongTensor(seq)


        # SORT YOUR TENSORS BY LENGTH!
        x_seq_lengths, perm_idx = x_seq_lengths.sort(0, descending=True)
        seq_tensor = seq_tensor[perm_idx]

        # utils.rnn lets you give (B,L,D) tensors where B is the batch size, L is the maxlength, if you use batch_first=True
        # Otherwise, give (L,B,D) tensors

    def iter_eval(self):
        x_batch = []
        y_batch = []
        
        for user in self.data:

            half = int(float(len(self.data[user])) / 2.0)

            x_batch.append(self.data[user][:half])
            # y_batch.append(int(self.data[user][half]))
            y_batch.append(self.data[user][half:])

            if len(y_batch) == self.batch_size:

                yield Variable(torch.cuda.LongTensor(x_batch)), y_batch
                
                x_batch = []
                y_batch = []

    def scatter(self, to_scatter):
        ret = [0] * self.hyper_params['max_user_hist']
        now = 0

        for i in to_scatter:
            ret[now] = i
            now += 1

        return ret