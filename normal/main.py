import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable

import numpy as np
import time
import pickle
import json
import gc
import datetime as dt

# from data_everything import load_data
from data import load_data
from model import Model
from utils import *
from hyper_params import *

def evaluate(reader):
    model.eval()
    total_loss = 0
    batch = 0

    ############# When to reinit hidden of the LSTM ?

    total, count = 0, 0
    mse = 0.0
    corr_eval, total_eval = 0, 0

    for x, y in reader.iter():
        batch += 1
        if batch > hyper_params['testing_batch_limit']: break
        
        output = model(x)
        loss = criterion(output, y)
        total_loss += loss.data

        # Evaluating Accuracy
        y = y.data

        # _, top = torch.sort(output)
        # top_k = top[:,-1].data
        top_k = output[:, 0].data

        # Because network predicts values from [0, 1], and y follows that
        y = y * 5
        top_k = top_k * 5

        mse += torch.sum(torch.pow((top_k - y).float(), 2))
        
        # matched = torch.nonzero(torch.eq(top_k, y))
        # if len(matched.size()) == 2: corr_eval += matched.size(0)
        
        total_eval += hyper_params['batch_size']

    return (total_loss[0]/batch) ** 0.5, (float(mse) / float(total_eval)) ** 0.5
    # return total_loss[0]/batch, (float(corr_eval) / float(total_eval)) * 100.0

def train(reader):
    model.train()
    total_loss = 0
    start_time = time.time()
    batch = 0

    ############# When to reinit hidden of the LSTM ?

    for x, y in reader.iter():
        batch += 1
        if batch > hyper_params['training_batch_limit']: return
        
        optimizer.zero_grad()

        output = model(x)

        # print(y)
        # print(output)
        # print("\n")

        loss = criterion(output, y)
        loss.backward()

        torch.nn.utils.clip_grad_norm(model.parameters(), hyper_params['exploding_clip'])
        optimizer.step()

        total_loss += loss.data

        if batch % hyper_params['batch_log_interval'] == 0 and batch > 0:
            cur_loss = (total_loss[0] / hyper_params['batch_log_interval']) ** 0.5
            elapsed = time.time() - start_time

            ss = '| epoch {:3d} | {:5d}/{:5d} batches | ms/batch {:5.2f} | loss {:5.4f}'.format(
                epoch, batch, int(min(train_reader.num_b, hyper_params['training_batch_limit'])),
                elapsed * 1000 / hyper_params['batch_log_interval'], cur_loss)
            
            file_write(hyper_params['log_file'], ss)

            total_loss = 0
            start_time = time.time()

train_reader, val_reader, test_reader, total_users, total_items = load_data(hyper_params)
hyper_params['total_users'] = total_users
hyper_params['total_items'] = total_items

file_write(hyper_params['log_file'], "\n\nSimulation run on: " + str(dt.datetime.now()) + "\n\n")
file_write(hyper_params['log_file'], "Data reading complete!")
file_write(hyper_params['log_file'], "Number of train batches: {:4d}".format(train_reader.num_b))
file_write(hyper_params['log_file'], "Number of test batches: {:4d}".format(test_reader.num_b))
file_write(hyper_params['log_file'], "Total Users: " + str(total_users))
file_write(hyper_params['log_file'], "Total Items: " + str(total_items) + "\n")

model = Model(hyper_params)
model.cuda()

criterion = nn.MSELoss()
# criterion = nn.CrossEntropyLoss(size_average=True)

optimizer = torch.optim.Adagrad(model.parameters(), lr=hyper_params['learning_rate'])
# optimizer = torch.optim.Adadelta(model.parameters())

file_write(hyper_params['log_file'], str(model))
file_write(hyper_params['log_file'], "\nModel Built!\nStarting Training...\n")

if hyper_params['data_parallel']: model = nn.DataParallel(model, dim=0).cuda()

best_val_loss = None
best_val_acc = None

try:
    for epoch in range(1, hyper_params['epochs'] + 1):
        epoch_start_time = time.time()
        train(train_reader)
        val_loss, val_acc = evaluate(val_reader)
        
        ss  = '-' * 89
        ss += '\n| end of epoch {:3d} | time: {:5.2f}s | valid loss {:5.4f} | val RMSE = {:5.4f}'.format(epoch, (time.time() - epoch_start_time), val_loss, val_acc)
        ss += '\n'
        ss += '-' * 89
        file_write(hyper_params['log_file'], ss)
        
        # if not best_val_acc or val_acc >= best_val_acc:
        if not best_val_loss or val_loss < best_val_loss:
            with open(hyper_params['model_file_name'], 'wb') as f: torch.save(model, f)
            best_val_loss = val_loss
            best_val_acc = val_acc

except KeyboardInterrupt: print('Exiting from training early')

with open(hyper_params['model_file_name'], 'rb') as f: model = torch.load(f)
test_loss, test_acc = evaluate(test_reader)

ss  = '=' * 89
ss += '\n| End of training | test loss {:5.4f} | test RMSE = {:5.4f}'.format(test_loss, test_acc)
ss += '\n'
ss += '=' * 89
file_write(hyper_params['log_file'], ss)