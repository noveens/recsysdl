import os
import sys
import random
reload(sys)
sys.setdefaultencoding("utf8")

from hyper_params import *
from utils import *





# Calculating min and max time for scaling later
f = open(hyper_params['data_base'] + 'ratings.dat')
lines = f.readlines()

max_time, min_time = 0, 1000000000000000000

for line in lines:
	temp = map(int, line.strip().split("::"))

	max_time = max(max_time, int(temp[3]))
	min_time = min(min_time, int(temp[3]))





# Calculating user and item histories
user_hist = {}
item_hist = {}
user_hist_items = {}

for line in lines:
	temp = map(int, line.strip().split("::"))

	if temp[0] not in user_hist: user_hist[temp[0]] = []
	if temp[0] not in user_hist_items: user_hist_items[temp[0]] = set()
	user_hist[temp[0]].append([temp[1], temp[2], float(temp[3] - min_time) / float(max_time - min_time)])
	user_hist_items[temp[0]].add(temp[1])

	if temp[1] not in item_hist: item_hist[temp[1]] = []
	item_hist[temp[1]].append([temp[0], temp[2], float(temp[3] - min_time) / float(max_time - min_time)])





# Sorting user and item histories
for user in user_hist:
	user_hist[user].sort(key=lambda x: x[2])

for item in item_hist:
	item_hist[item].sort(key=lambda x: x[2])





# Calculating the train and test splits, along with randomly sampled negative examples
train = {}
test = {}

users_kept = 0

for user in user_hist:

	if len(user_hist[user]) > hyper_params['max_user_hist']: continue
	if len(user_hist[user]) < hyper_params['min_user_hist']: continue
	
	users_kept += 1

	train_split = user_hist[user][:-1]
	test_split = user_hist[user][-1]

	train[user] = train_split
	test[user] = test_split		

print("users_kept = " + str(users_kept))





# There are missing movie numbers (Max movie number > num of movies that have been watched)
f = open(hyper_params['data_base'] + 'movies.dat')
lines = f.readlines()
max_item_no = 0

for line in lines:
	temp = line.strip().split("::")
	max_item_no = max(max_item_no, int(temp[0]))

for i in range(max_item_no):
	if i not in item_hist: item_hist[i] = []





# Saving all objects
save_obj_json(train, hyper_params['data_base'] + "train_classification")
save_obj_json(test, hyper_params['data_base'] + "test_classification")
save_obj_json(item_hist, hyper_params['data_base'] + "item_hist_classification")
save_obj_json(user_hist, hyper_params['data_base'] + "user_hist_classification")
