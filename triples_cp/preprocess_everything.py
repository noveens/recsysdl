import os
import sys
# import importlib
# importlib.reload(sys)
# sys.setdefaultencoding("utf8")

from tqdm import tqdm

from hyper_params import *
from utils import *

train = {}
val = {}
test = {}

f = open(hyper_params['data_base'] + 'ratings.dat')
lines = f.readlines()

max_time, min_time = 0, 1000000000000000000

for line in lines:
    temp = list(map(int, line.strip().split("::")))
    max_time = max(max_time, int(temp[3]))
    min_time = min(min_time, int(temp[3]))

user_hist = {}
item_hist = {}

for line in lines:
    temp = list(map(int, line.strip().split("::")))

    if temp[0] not in user_hist: user_hist[temp[0]] = []
    user_hist[temp[0]].append([temp[1], temp[2], float(temp[3] - min_time) / float(max_time - min_time)])

    if temp[1] not in item_hist: item_hist[temp[1]] = []
    item_hist[temp[1]].append([temp[0], temp[2], float(temp[3] - min_time) / float(max_time - min_time)])

f = open(hyper_params['data_base'] + 'movies.dat', encoding="ISO-8859-1")
lines = f.readlines()
max_item_no = 0

for line in lines:
    temp = line.strip().split("::")
    max_item_no = max(max_item_no, int(temp[0]))

for i in range(max_item_no):
    if i not in item_hist: item_hist[i] = []

for user in user_hist:
    user_hist[user].sort(key=lambda x: x[2])

for item in item_hist:
    item_hist[item].sort(key=lambda x: x[2])

split = hyper_params['data_split']

for user in user_hist:

    train_split = user_hist[user][:int(split[0] * len(user_hist[user]))]
    val_split = user_hist[user][int(split[0] * len(user_hist[user])) : int((split[0] + split[1]) * len(user_hist[user]))]
    test_split = user_hist[user][int((split[0] + split[1]) * len(user_hist[user])):]

    train[user] = train_split
    val[user] = val_split
    test[user] = test_split

print("Started saving primitive files")
save_obj_json(train, hyper_params['data_base'] + "train")
save_obj_json(val, hyper_params['data_base'] + "val")
save_obj_json(test, hyper_params['data_base'] + "test")
save_obj_json(item_hist, hyper_params['data_base'] + "item_hist")
save_obj_json(user_hist, hyper_params['data_base'] + "user_hist")
print("Saved primitive files")

def scatter(to_scatter, is_user, is_int):
    if is_user == True: max_rows = hyper_params['user_hist_size']
    else: max_rows = hyper_params['item_hist_size']

    if is_int == True:
        scattered = [0] * max_rows
        now = 0
        for i in to_scatter:
            scattered[now] = int(i)
            now += 1
            if now >= max_rows: break
        return scattered

    else:
        scattered = [0.0] * max_rows
        now = 0
        for i in to_scatter:
            scattered[now] = float(i)
            now += 1
            if now >= max_rows: break
        return scattered

def find_i_hist(item, time):
    hist = []
    hist_rating = []
    hist_time = []
    time = float(time)

    for i in item_hist[int(item)]:
        if float(i[2]) < time:
            hist.append(i[0])
            hist_rating.append(i[1])
            hist_time.append(i[2])

        else:
            return hist, hist_rating, hist_time

count_now = 0
for data in [train, val, test]:
    x_tuples = []
    y_tuples = []

    print("Started Making Tuples: " + str(count_now))

    x_batch_u_hist, x_batch_u_hist_time, x_batch_u_hist_rating = [], [], []
    x_batch_i_hist, x_batch_i_hist_time, x_batch_i_hist_rating = [], [], []
    x_batch_user = []
    x_batch_item = []
    y_batch = []

    all_keys = list(data.keys())

    for iii in tqdm(range(len(all_keys))):
        user = all_keys[iii]
        for i in range(len(data[user])):

            u_hist, u_hist_rating, u_hist_time = [], [], []

            for j in data[user][:i]:
                u_hist.append(j[0])
                u_hist_rating.append(j[1])
                u_hist_time.append(j[2])

            i_hist, i_hist_rating, i_hist_time = find_i_hist(data[user][i][0], data[user][i][2]) # Has users

            x_batch_u_hist.append(scatter(u_hist, True, True))
            x_batch_u_hist_rating.append(scatter(u_hist_rating, True, True))
            x_batch_u_hist_time.append(scatter(u_hist_time, True, False))

            x_batch_i_hist.append(scatter(i_hist, False, True))
            x_batch_i_hist_rating.append(scatter(i_hist_rating, False, True))
            x_batch_i_hist_time.append(scatter(i_hist_time, False, False))

            x_batch_user.append(int(user))
            x_batch_item.append(int(data[user][i][0]))
            
            y_batch.append(float(data[user][i][1])) # Scale is [1, 5]

            if len(y_batch) == hyper_params['batch_size']:
                x_tuples.append([
                    x_batch_i_hist,
                    x_batch_i_hist_rating, 
                    x_batch_i_hist_time,

                    x_batch_u_hist,
                    x_batch_u_hist_rating, 
                    x_batch_u_hist_time,
                    
                    x_batch_user, 
                    x_batch_item
                ])

                y_tuples.append(
                    y_batch
                )
                
                x_batch_u_hist, x_batch_u_hist_time, x_batch_u_hist_rating = [], [], []
                x_batch_i_hist, x_batch_i_hist_time, x_batch_i_hist_rating = [], [], []
                x_batch_user = []
                x_batch_item = []
                y_batch = []

    print("Finished Making Tuples and started saving: " + str(count_now))

    if count_now == 0:
        save_obj([x_tuples, y_tuples], hyper_params['data_base'] + "train_tuples_100")
    elif count_now == 1:
        save_obj([x_tuples, y_tuples], hyper_params['data_base'] + "val_tuples_100")
    elif count_now == 2:
        save_obj([x_tuples, y_tuples], hyper_params['data_base'] + "test_tuples_100")

    print("Finished Saving Tuples: " + str(count_now))
    count_now += 1

print("Done!")