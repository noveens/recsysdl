import numpy as np
import torch
from torch.autograd import Variable

from utils import *

def load_data(hyper_params):
    
    file_write(hyper_params['log_file'], "Started reading data file")
    
    train = load_obj_json(hyper_params['data_base'] + 'train_normal')
    val = load_obj_json(hyper_params['data_base'] + 'val_normal')
    test = load_obj_json(hyper_params['data_base'] + 'test_normal')
    user_hist = load_obj_json(hyper_params['data_base'] + 'user_hist_normal')
    item_hist = load_obj_json(hyper_params['data_base'] + 'item_hist_normal')
    
    file_write(hyper_params['log_file'], "Data Files loaded!")

    train_reader = DataReader(hyper_params, train, len(user_hist), item_hist)
    val_reader = DataReader(hyper_params, val, len(user_hist), item_hist)
    test_reader = DataReader(hyper_params, test, len(user_hist), item_hist)

    return train_reader, val_reader, test_reader, len(user_hist), len(item_hist)

class DataReader:

    def __init__(self, hyper_params, data, num_users, item_hist):
        self.hyper_params = hyper_params
        self.batch_size = hyper_params['batch_size']
        self.item_hist = item_hist
        self.num_users = num_users
        self.num_items = len(item_hist)
        self.data = data

        self.number()

        self._x_batches = []
        self._y_batches = []

    def number(self):
        y_batch = []

        count = 0

        for user in self.data:
            y_batch = []
            for i in range(len(self.data[user])):
                y_batch.append(0)

                if len(y_batch) == self.batch_size:
                    count += 1
                    y_batch = []

        self.num_b = count

    def iter(self):
        for user in self.data:

            x_batch_u_hist, x_batch_u_hist_time, x_batch_u_hist_rating = [], [], []
            x_batch_i_hist, x_batch_i_hist_time, x_batch_i_hist_rating = [], [], []
            x_batch_user = []
            x_batch_item = []
            y_batch = []

            for i in range(len(self.data[user])):

                u_hist, u_hist_rating, u_hist_time = [], [], []

                for j in self.data[user][:i]:
                    u_hist.append(j[0])
                    u_hist_rating.append(float(j[1]) / float(5))
                    u_hist_time.append(j[2])

                i_hist, i_hist_rating, i_hist_time = self.find_i_hist(self.data[user][i][0], self.data[user][i][2]) # Has users

                x_batch_u_hist.append(self.scatter(u_hist, True, True))
                x_batch_u_hist_rating.append(self.scatter(u_hist_rating, True, True))
                x_batch_u_hist_time.append(self.scatter(u_hist_time, True, False))

                x_batch_i_hist.append(self.scatter(i_hist, False, True))
                x_batch_i_hist_rating.append(self.scatter(i_hist_rating, False, True))
                x_batch_i_hist_time.append(self.scatter(i_hist_time, False, False))

                x_batch_user.append(int(user))
                x_batch_item.append(int(self.data[user][i][0]))

                y_batch.append(float(self.data[user][i][1]) / float(5)) # Scale is [1, 5]
                    
                if len(y_batch) == self.batch_size:

                    yield [
                        Variable(torch.cuda.LongTensor(x_batch_u_hist)),
                        Variable(torch.cuda.FloatTensor(x_batch_u_hist_rating)), 
                        Variable(torch.cuda.FloatTensor(x_batch_u_hist_time)),
                        
                        Variable(torch.cuda.LongTensor(x_batch_i_hist)),
                        Variable(torch.cuda.FloatTensor(x_batch_i_hist_rating)), 
                        Variable(torch.cuda.FloatTensor(x_batch_i_hist_time)),

                        Variable(torch.cuda.LongTensor(x_batch_user)), 
                        Variable(torch.cuda.LongTensor(x_batch_item))
                    ], Variable(torch.cuda.FloatTensor(y_batch))
                    
                    x_batch_u_hist, x_batch_u_hist_time, x_batch_u_hist_rating = [], [], []
                    x_batch_i_hist, x_batch_i_hist_time, x_batch_i_hist_rating = [], [], []
                    x_batch_user = []
                    x_batch_item = []
                    y_batch = []

    def scatter(self, to_scatter, is_user, is_int):
        if is_user == True: max_rows = self.hyper_params['user_hist_size']
        else: max_rows = self.hyper_params['item_hist_size']

        if is_int == True:
            scattered = [0] * max_rows
            now = 0
            for i in to_scatter:
                scattered[now] = int(i)
                now += 1
                if now >= max_rows: break
            return scattered

        else:
            scattered = [0.0] * max_rows
            now = 0
            for i in to_scatter:
                scattered[now] = float(i)
                now += 1
                if now >= max_rows: break
            return scattered

    def find_i_hist(self, item, time):
        hist = []
        hist_rating = []
        hist_time = []
        time = float(time)

        for i in self.item_hist[str(item)]:
            if float(i[2]) < time:
                hist.append(i[0])
                hist_rating.append(float(i[1]) / float(5))
                hist_time.append(i[2])
            else:
                return hist, hist_rating, hist_time
