import numpy as np
import random
import torch
from torch.autograd import Variable
from tqdm import tqdm

from utils import *

'''
Main function called in the main.py file

hyper_params: object having all hyper parameters

returns: object of class DataReader for train data ; test array ; number of users ; number of items
'''
def load_data(hyper_params):
    
    file_write(hyper_params['log_file'], "Started reading data file")
    
    train = load_obj_json(hyper_params['data_base'] + 'train_triples_simple')
    test = load_obj_json(hyper_params['data_base'] + 'test_triples_simple')
    user_hist = load_obj_json(hyper_params['data_base'] + 'user_hist_triples_simple')
    item_hist = load_obj_json(hyper_params['data_base'] + 'item_hist_triples_simple')
    bad_set = load_obj_json(hyper_params['data_base'] + 'bad_set_triples_simple')
    good_set = load_obj_json(hyper_params['data_base'] + 'good_set_triples_simple')

    file_write(hyper_params['log_file'], "Data Files loaded!")

    train_reader = DataReader(hyper_params, train, len(user_hist), item_hist, bad_set, good_set, True)
    test_reader = DataReader(hyper_params, test, len(user_hist), item_hist, bad_set, good_set, False)

    return train_reader, test_reader, len(user_hist), len(item_hist)

'''
Class which manages train and test data
Has a function iter which yields the x_batch, y_batch

hyper_params: object having all hyper parameters
data        : training data
num_users   : total number of users
item_hist   : object having all item history (containing users)
'''
class DataReader:

    def __init__(self, hyper_params, data, num_users, item_hist, bad_set, good_set, is_training):
        self.hyper_params = hyper_params
        self.batch_size = hyper_params['batch_size']
        self.item_hist = item_hist
        self.num_users = num_users
        self.num_items = len(item_hist)
        self.data = data
        self.bad_set = bad_set
        self.good_set = good_set
        self.is_training = is_training
        self.all_users = []

        self.number_users()
        self.number()

        self._x_batches = []
        self._y_batches = []

    '''
    Counts the number of batches
    '''
    def number(self):
        users_done = 0
        count = 0
        y_batch = []

        for user in self.data:

            if users_done > self.hyper_params['number_users_to_keep']: break
            users_done += 1

            for i in range(len(self.data[user])):
                for ii in range(i+1, len(self.data[user])):

                    if self.data[user][i][1] == self.data[user][ii][1]: continue

                    y_batch.append(0)
                    y_batch.append(0)

                    if len(y_batch) == self.batch_size:
                        y_batch = []
                        count += 1

            # if self.is_training == True:
            #     for i in range(len(self.good_set[user])):
            #         for ii in range(len(self.bad_set[user])):

            #             y_batch.append(1.0)
            #             y_batch.append(0.0)

            #             if len(y_batch) == self.batch_size:
            #                 y_batch = []
            #                 count += 1

        self.num_b = count

    '''
    Counts the number of batches
    '''
    def number_users(self):
        users_done = 0
        count = 0

        for user in self.data:

            if users_done > self.hyper_params['number_users_to_keep']: break
            users_done += 1

            y_batch = []

            for i in range(len(self.data[user])):
                for ii in range(i+1, len(self.data[user])):

                    if self.data[user][i][1] == self.data[user][ii][1]: continue

                    y_batch.append(0)
                    y_batch.append(0)

            if len(y_batch) > 0:
                count += 1
                self.all_users.append(user)

        self.num_u = count

    '''
    Yields x_batch and y_batch at each time step

    returns: [
        x_batch_user, (users)
        x_batch_item (items)
    ], y_batch (rating given by x_batch_user to x_batch_item)
    '''
    def iter(self):
        keep_chance = self.hyper_params['training_random_keeping_chance']
        users_done = 0

        x_batch_user = []
        x_batch_item = []
        y_batch = []

        for user in self.data:

            if users_done > self.hyper_params['number_users_to_keep']: break
            users_done += 1

            for i in range(len(self.data[user])):
                for ii in range(i+1, len(self.data[user])):

                    if self.data[user][i][1] == self.data[user][ii][1]: continue

                    random_toss = random.uniform(0, 1)
                    if random_toss > keep_chance: continue

                    x_batch_user.append(int(user))
                    x_batch_user.append(int(user))

                    item_1 = self.data[user][i][0]
                    item_2 = self.data[user][ii][0]
                    r1 = float(self.data[user][i][1]) / float(5)
                    r2 = float(self.data[user][ii][1]) / float(5)

                    if self.data[user][i][1] < self.data[user][ii][1]:
                        temp = item_1
                        item_1 = item_2
                        item_2 = temp

                        temp2 = r1
                        r1 = r2
                        r2 = temp2

                    x_batch_item.append(int(item_1))
                    x_batch_item.append(int(item_2))

                    y_batch.append(r1)
                    y_batch.append(r2)

                    if len(y_batch) == self.batch_size:

                        yield [
                            Variable(LongTensor(x_batch_user)), 
                            Variable(LongTensor(x_batch_item))
                        ], Variable(FloatTensor(y_batch))
                        
                        x_batch_user = []
                        x_batch_item = []
                        y_batch = []

            # if self.is_training == True:
            #     for i in range(len(self.good_set[user])):
            #         for ii in range(len(self.bad_set[user])):

            #             x_batch_user.append(int(user))
            #             x_batch_user.append(int(user))

            #             x_batch_item.append(int(self.good_set[user][i]))
            #             x_batch_item.append(int(self.bad_set[user][ii]))

            #             y_batch.append(1.0)
            #             y_batch.append(0.0)

            #             if len(y_batch) == self.batch_size:

            #                 yield [
            #                     Variable(LongTensor(x_batch_user)), 
            #                     Variable(LongTensor(x_batch_item))
            #                 ], Variable(FloatTensor(y_batch))
                            
            #                 x_batch_user = []
            #                 x_batch_item = []
            #                 y_batch = []

    def iter_eval(self, is_train_set):
        users_done = 0

        temp3 = len(self.all_users)
        if is_train_set == True: temp3 = min(temp3, self.hyper_params['user_limit_inversion'])

        for jj in tqdm(range(temp3)):
            user = self.all_users[jj]

            if users_done > self.hyper_params['number_users_to_keep']: break
            users_done += 1

            x_batch_user = []
            x_batch_item = []
            y_batch = []
            all_movies = []

            # tmp = [[i[0], i[1]] for i in self.data[user]]
            # tmp = sorted(tmp, key=lambda x: x[1])
            # print(tmp)

            for i in range(len(self.data[user])):
                all_movies.append(self.data[user][i][0])

                for ii in range(i+1, len(self.data[user])):

                    if self.data[user][i][1] == self.data[user][ii][1]: continue

                    x_batch_user.append(int(user))
                    x_batch_user.append(int(user))

                    item_1 = self.data[user][i][0]
                    item_2 = self.data[user][ii][0]
                    r1 = float(self.data[user][i][1]) / float(5)
                    r2 = float(self.data[user][ii][1]) / float(5)

                    if self.data[user][i][1] < self.data[user][ii][1]:
                        temp = item_1
                        item_1 = item_2
                        item_2 = temp

                        temp2 = r1
                        r1 = r2
                        r2 = temp2

                    x_batch_item.append(int(item_1))
                    x_batch_item.append(int(item_2))

                    y_batch.append(r1)
                    y_batch.append(r2)

            if len(y_batch) > 0:
                yield [
                    Variable(LongTensor(x_batch_user)), 
                    Variable(LongTensor(x_batch_item))
                ], Variable(FloatTensor(y_batch)), all_movies
