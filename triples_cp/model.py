import math
import torch
import random
import numpy as np
from torch import nn
from torch.autograd import Variable
import torch.nn.functional as F

class Attention(nn.Module):
    def __init__(self, in_size):
        super(Attention, self).__init__()
        # self.attention_weights = nn.Parameter(torch.FloatTensor(in_size), requires_grad = True)
        self.attention_dense_layer = nn.Linear(in_size, 1)
        self.attention_dropout = nn.Dropout(0.2)
        # self.attention_activation = nn.Tanh()
        self.attention_activation = nn.ReLU()
        self.softmax = nn.Softmax(1)

    def forward(self, hidden_states):
        shape = hidden_states.shape
        scores = self.attention_dropout(self.attention_activation(self.attention_dense_layer(hidden_states.contiguous().view(-1, shape[-1])).view(-1, shape[1])))
        scores = self.softmax(scores)
        # print(scores)
        # print(torch.sum(scores, 1))
        return scores.unsqueeze(1).bmm(hidden_states).squeeze(1)
        # print(final)
        # print("\n\n")
    
    def forward_parameter(self, hidden_states):
        scores = self.attention_activation(hidden_states.matmul(self.attention_weights))
        scores = self.softmax(scores)
        # print(scores)
        # print(torch.sum(scores, 1))
        weighted = torch.mul(hidden_states, scores.unsqueeze(-1).expand_as(hidden_states))
        # print(weighted.sum(1).squeeze())
        # print("\n\n")
        return weighted.sum(1).squeeze()

class Model(nn.Module):
    def __init__(self, hyper_params):
        super(Model, self).__init__()
        self.hyper_params = hyper_params

        self.user_embed = nn.Embedding(hyper_params['total_users'] + 1, hyper_params['user_embed_size'])
        self.item_embed = nn.Embedding(hyper_params['total_items'] + 1, hyper_params['item_embed_size'])

        self.user_rnn = nn.GRU(hyper_params['item_embed_size'], hyper_params['user_rnn_size'], hyper_params['user_rnn_layers'], dropout=hyper_params['user_rnn_dropout'], bidirectional=hyper_params['user_rnn_bi'], batch_first=True)
        self.item_rnn = nn.GRU(hyper_params['user_embed_size'], hyper_params['item_rnn_size'], hyper_params['item_rnn_layers'], dropout=hyper_params['item_rnn_dropout'], bidirectional=hyper_params['item_rnn_bi'], batch_first=True)
        
        if hyper_params['user_attention']:
            self.user_attention = Attention(hyper_params['user_rnn_size'])

        if hyper_params['item_attention']:
            self.item_attention = Attention(hyper_params['item_rnn_size'])

        self.activation = nn.ReLU()
        self.activation_last = nn.Sigmoid()
        
        prev = hyper_params['user_rnn_size'] + hyper_params['item_rnn_size'] + hyper_params['user_embed_size'] + hyper_params['item_embed_size']

        self.layer1 = nn.Linear(prev, 64)
        self.drop1  = nn.Dropout(0.2)
        self.layer2 = nn.Linear(64, 16)
        self.drop2  = nn.Dropout(0.2)
        self.layer3 = nn.Linear(16, 4)
        self.layer4 = nn.Linear(4, 1)

    def init_hidden(self, batch_size, bi, layers, size):
        mul = 1
        if bi == True: mul = 2
        # return Variable(torch.zeros(mul * layers, batch_size, size).cuda(), requires_grad=True)
        return Variable(torch.randn(mul * layers, batch_size, size).cuda(), requires_grad=True)

    def repack(self, h):
        if type(h) == Variable:
            return Variable(h.data.cuda(), requires_grad=True)
        else:
            return tuple(repackage_hidden(v) for v in h)

    def forward(self, src):

        user_hist = self.item_embed(src[0])          # [bsz x history_len x user_embed_size]        
        item_hist = self.user_embed(src[1])          # [bsz x history_len x item_embed_size]
        user = self.user_embed(src[2])               # [bsz x user_embed_size]
        item = self.item_embed(src[3])               # [bsz x item_embed_size]

        # self.user_rnn_hidden = self.repack(self.user_rnn_hidden)
        self.user_rnn_hidden = self.init_hidden(user_hist.size(0), self.hyper_params['user_rnn_bi'], self.hyper_params['user_rnn_layers'], self.hyper_params['user_rnn_size'])
        self.user_rnn.flatten_parameters()
        user_rnn_output, self.user_rnn_hidden = self.user_rnn(user_hist, self.user_rnn_hidden) # [bsz x user_rnn_size]
        
        # self.item_rnn_hidden = self.repack(self.item_rnn_hidden)
        self.item_rnn_hidden = self.init_hidden(item_hist.size(0), self.hyper_params['item_rnn_bi'], self.hyper_params['item_rnn_layers'], self.hyper_params['item_rnn_size'])
        self.item_rnn.flatten_parameters()
        item_rnn_output, self.item_rnn_hidden = self.item_rnn(item_hist, self.item_rnn_hidden) # [bsz x item_rnn_size]

        if self.hyper_params['user_attention'] == True: user_attention_output = self.user_attention(user_rnn_output)
        else: user_attention_output = user_rnn_output[:, -1, :]

        if self.hyper_params['item_attention'] == True: item_attention_output = self.item_attention(item_rnn_output)
        else: item_attention_output = item_rnn_output[:, -1, :]

        output_user = torch.cat([user_attention_output, user], dim = -1) # .cuda()
        output_item = torch.cat([item_attention_output, item], dim = -1) # .cuda()

        output = torch.cat([output_user, output_item], dim = -1) # .cuda()

        output = self.activation(self.layer1(output))
        output = self.drop1(output)
        output = self.activation(self.layer2(output))
        output = self.drop2(output)
        output = self.activation(self.layer3(output))
        output = self.activation_last(self.layer4(output))

        return output # [bsz x last_layer_dim]

    def forward_with_rating_and_time(self, src):

        user_hist = self.item_embed(src[0])          # [bsz x history_len x user_embed_size]
        user_hist_rating = src[1]                    # [bsz x history_len]
        user_hist_time = src[2]                      # [bsz x history_len]
        
        item_hist = self.user_embed(src[3])          # [bsz x history_len x item_embed_size]
        item_hist_rating = src[4]                    # [bsz x history_len]
        item_hist_time = src[5]                      # [bsz x history_len]

        user = self.user_embed(src[6])               # [bsz x user_embed_size]
        item = self.item_embed(src[7])               # [bsz x item_embed_size]

        user_rnn_input = torch.cat([user_hist_time.unsqueeze(-1), user_hist_rating.unsqueeze(-1)], dim = -1)
        user_rnn_input = torch.cat([user_rnn_input, user_hist], dim = -1)

        # self.user_rnn_hidden = self.repack(self.user_rnn_hidden)
        self.user_rnn_hidden = self.init_hidden(user_hist.size(0), self.hyper_params['user_rnn_bi'], self.hyper_params['user_rnn_layers'], self.hyper_params['user_rnn_size'])
        self.user_rnn.flatten_parameters()
        user_rnn_output, self.user_rnn_hidden = self.user_rnn(user_rnn_input, self.user_rnn_hidden) # [bsz x user_rnn_size]

        item_rnn_input = torch.cat([item_hist_time.unsqueeze(-1), item_hist_rating.unsqueeze(-1)], dim = -1)
        item_rnn_input = torch.cat([item_rnn_input, item_hist], dim = -1)
        
        # self.item_rnn_hidden = self.repack(self.item_rnn_hidden)
        self.item_rnn_hidden = self.init_hidden(item_hist.size(0), self.hyper_params['item_rnn_bi'], self.hyper_params['item_rnn_layers'], self.hyper_params['item_rnn_size'])
        self.item_rnn.flatten_parameters()
        item_rnn_output, self.item_rnn_hidden = self.item_rnn(item_rnn_input, self.item_rnn_hidden) # [bsz x item_rnn_size]

        if self.hyper_params['user_attention'] == True: user_attention_output = self.user_attention(user_rnn_output)
        else: user_attention_output = user_rnn_output[:, -1, :]

        if self.hyper_params['item_attention'] == True: item_attention_output = self.item_attention(item_rnn_output)
        else: item_attention_output = item_rnn_output[:, -1, :]

        output_user = torch.cat([user_attention_output, user], dim = -1) # .cuda()
        output_item = torch.cat([item_attention_output, item], dim = -1) # .cuda()

        output = torch.cat([output_user, output_item], dim = -1) # .cuda()

        output = self.activation(self.layer1(output))
        output = self.drop1(output)
        output = self.activation(self.layer2(output))
        output = self.drop2(output)
        output = self.activation(self.layer3(output))
        output = self.activation_last(self.layer4(output))

        return output # [bsz x last_layer_dim]
