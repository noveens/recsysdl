import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable

import numpy as np
import time
import random
import pickle
import json
import gc
import datetime as dt

from data import load_data
from model import Model
from utils import *
from hyper_params import *
from custom_loss import HingeLoss
from evaluation import evaluate

def train(reader):
    model.train()
    total_loss = 0
    start_time = time.time()
    batch = 0
    batch_limit = int(min(train_reader.num_b, hyper_params['training_batch_limit']))

    for x, y in reader.iter():
        batch += 1
        if batch > hyper_params['training_batch_limit']: return
        
        model.zero_grad()
        optimizer.zero_grad()

        output = model(x)
        
        loss = criterion(output, y)
        loss.backward()

        torch.nn.utils.clip_grad_norm(model.parameters(), hyper_params['exploding_clip'])
        optimizer.step()

        total_loss += loss.data

        if (batch % hyper_params['batch_log_interval'] == 0 and batch > 0) or batch == batch_limit:
            div = hyper_params['batch_log_interval']
            if batch == batch_limit: div = (batch_limit % hyper_params['batch_log_interval']) - 1

            cur_loss = (total_loss[0] / div)
            elapsed = time.time() - start_time

            ss = '| epoch {:3d} | {:5d}/{:5d} batches | ms/batch {:5.2f} | loss {:5.4f}'.format(
                epoch, batch, batch_limit,
                elapsed * 1000 / div, cur_loss)
            
            file_write(hyper_params['log_file'], ss)

            total_loss = 0
            start_time = time.time()

train_reader, test_reader, total_users, total_items = load_data(hyper_params)
hyper_params['total_users'] = total_users
hyper_params['total_items'] = total_items
hyper_params['testing_batch_limit'] = test_reader.num_b

file_write(hyper_params['log_file'], "\n\nSimulation run on: " + str(dt.datetime.now()) + "\n\n")
file_write(hyper_params['log_file'], "Data reading complete!")
file_write(hyper_params['log_file'], "Number of train batches: {:4d}".format(train_reader.num_b))
file_write(hyper_params['log_file'], "Number of test batches: {:4d}".format(test_reader.num_b))
file_write(hyper_params['log_file'], "Total Users: " + str(total_users))
file_write(hyper_params['log_file'], "Total Items: " + str(total_items) + "\n")

model = Model(hyper_params)
if is_cuda_available: model.cuda()

criterion = HingeLoss(
    hyper_params['batch_size'], hyper_params['loss_type'], hyper_params['m_loss']
)

if hyper_params['optimizer'] == 'adagrad':
    optimizer = torch.optim.Adagrad(model.parameters(), lr=hyper_params['learning_rate'], weight_decay=hyper_params['weight_decay'])
elif hyper_params['optimizer'] == 'adadelta':
    optimizer = torch.optim.Adadelta(model.parameters(), weight_decay=hyper_params['weight_decay'])
elif hyper_params['optimizer'] == 'adam':
    optimizer = torch.optim.Adam(model.parameters(), lr=hyper_params['learning_rate'], weight_decay=hyper_params['weight_decay'])
elif hyper_params['optimizer'] == 'rmsprop':
    optimizer = torch.optim.RMSprop(model.parameters(), lr=hyper_params['learning_rate'], weight_decay=hyper_params['weight_decay'])


file_write(hyper_params['log_file'], str(model))
file_write(hyper_params['log_file'], "\nModel Built!\nStarting Training...\n")

if hyper_params['data_parallel']: 
    model = nn.DataParallel(model, dim=0)
    if is_cuda_available: model = model.cuda()

best_val_hr = None

try:
    for epoch in range(1, hyper_params['epochs'] + 1):
        epoch_start_time = time.time()
        train(train_reader)
        
        metrics = evaluate(model, criterion, train_reader, hyper_params, True)
        
        string = ""
        for m in metrics:
            string += " | " + m + ' = ' + str(metrics[m])
        string += ' (TRAIN)'

        metrics = evaluate(model, criterion, test_reader, hyper_params, False)
        
        string2 = ""
        for m in metrics:
            string2 += " | " + m + ' = ' + str(metrics[m])
        string2 += ' (TEST)'

        ss  = '-' * 89
        ss += '\n| end of epoch {:3d} | time: {:5.2f}s'.format(epoch, (time.time() - epoch_start_time))
        ss += string
        ss += '\n'
        ss += '-' * 89
        ss += '\n| end of epoch {:3d} | time: {:5.2f}s'.format(epoch, (time.time() - epoch_start_time))
        ss += string2
        ss += '\n'
        ss += '-' * 89
        file_write(hyper_params['log_file'], ss)
        
        if not best_val_hr or metrics['CP'] >= best_val_hr:
            with open(hyper_params['model_file_name'], 'wb') as f: torch.save(model, f)
            best_val_hr = metrics['CP']

except KeyboardInterrupt: print('Exiting from training early')

with open(hyper_params['model_file_name'], 'rb') as f: model = torch.load(f)
metrics = evaluate(model, criterion, test_reader, hyper_params, False)

string = ""
for m in metrics:
    string += " | " + m + ' = ' + str(metrics[m])

ss  = '=' * 89
ss += '\n| End of training'
ss += string
ss += '\n'
ss += '=' * 89
file_write(hyper_params['log_file'], ss)