import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable

import numpy as np
import time
import random
import pickle
import json
import gc
import datetime as dt
import functools

from data import load_data
from model import Model
from utils import *
from hyper_params import *
from custom_loss import HingeLoss

def mergeSortInversions(arr, y_pair_map):
    if len(arr) == 1:
        return arr, 0
    else:
        a = arr[:int(len(arr)/2)]
        b = arr[int(len(arr)/2):]

        a, ai = mergeSortInversions(a, y_pair_map)
        b, bi = mergeSortInversions(b, y_pair_map)
        c = []

        i = 0
        j = 0
        inversions = 0 + ai + bi

    while i < len(a) and j < len(b):
        m1 = a[i]
        m2 = b[j]

        if m2 in y_pair_map[m1] and y_pair_map[m1][m2] == 1:
            c.append(b[j])
            j += 1
            inversions += (len(a)-i)
        else:
            c.append(a[i])
            i += 1

    c += a[i:]
    c += b[j:]

    return c, inversions

def evaluate_inversions(reader, is_train_set):
    model.eval()

    ret = 0.0
    
    NDCG = {}
    total_NDCG = {}

    Ks = [1, 5, 10, 15, 20]

    for k in Ks:
        NDCG[str(k)] = 0.0
        total_NDCG[str(k)] = 0.0
    
    # NDCG['5'] = 0.0
    # NDCG['10'] = 0.0
    # NDCG['20'] = 0.0

    # total_NDCG['5'] = 0.0
    # total_NDCG['10'] = 0.0
    # total_NDCG['20'] = 0.0

    total = 0
    user_done = 0

    for x, y, all_movies in reader.iter_eval(is_train_set):
        user_done += 1
        if is_train_set == True and user_done >= hyper_params['user_limit_inversion']: break

        output = model(x)
        output = output[:,0]

        first_indices = LongTensor(range(0, x[0].shape[0], 2))
        second_indices = LongTensor(range(1, x[0].shape[0], 2))

        output_first = output[first_indices]
        output_second = output[second_indices]

        y_first = y[first_indices]
        y_second = y[second_indices]

        x_first = x[1][first_indices]
        x_second = x[1][second_indices]

        y_diff = torch.gt(y_first, y_second).float() - torch.gt(y_second, y_first).float()
        out_diff = torch.gt(output_first, output_second).float() - torch.gt(output_second, output_first).float()

        # print(y_diff)
        # print(out_diff)
        # print()

        y_pair_map = {}
        out_pair_map = {}
        y_r = {}
        for i in range(len(x_first)):

            m1 = int(x_first[i])
            m2 = int(x_second[i])

            y_r[str(m1)] = round(float(y_first[i].data), 2)
            y_r[str(m2)] = round(float(y_second[i].data), 2)

            if m1 not in y_pair_map: y_pair_map[m1] = {}
            if m2 not in y_pair_map: y_pair_map[m2] = {}
            if m1 not in out_pair_map: out_pair_map[m1] = {}
            if m2 not in out_pair_map: out_pair_map[m2] = {}

            y_pair_map[m1][m2] = int(y_diff[i])
            y_pair_map[m2][m1] = -1 * int(y_diff[i])

            out_pair_map[m1][m2] = int(out_diff[i])
            out_pair_map[m2][m1] = -1 * int(out_diff[i])

        # print(y_pair_map)
        # print(out_pair_map)
        # print()

        def compare_out(item1, item2):
            if item2 not in out_pair_map[item1]: return 0
            return out_pair_map[item1][item2]
        
        # print(all_movies)

        all_movies = sorted(all_movies, key=functools.cmp_to_key(compare_out))

        # print(all_movies)

        _, errors = mergeSortInversions(all_movies, y_pair_map)
        total2 = (len(all_movies) * (len(all_movies) - 1)) / 2
        
        # errors2 = 0
        # for i in range(len(all_movies)):
        #     for j in range(i+1, len(all_movies)):
        #         m1 = all_movies[i]
        #         m2 = all_movies[j]
        #         if m2 in y_pair_map[m1] and y_pair_map[m1][m2] == 1: errors2 += 1
        #         total2 += 1

        # assert errors == errors2

        # print(errors)
        # exit(0)
        
        errors = float(errors) / float(total2)
        ret += errors
        total += 1

        all_movies.reverse()
        final = []
        final_sorted = []
        for i in all_movies: 
            final.append([i, y_r[str(i)]])
            final_sorted.append([i, y_r[str(i)]])
        final_sorted = sorted(final_sorted, key=lambda x: x[1])
        final_sorted.reverse()

        # print(final)
        # print(final_sorted)
        # print()

        # Calculate NDCG
        for k in Ks:
            if k <= len(final):
                out = final[:k]
                out_sorted = final_sorted[:k]

                now = 0.0
                now_best = 0.0
                for i in range(k):
                    now += float(out[i][1]) / float(np.log2(i+2))
                    now_best += float(out_sorted[i][1]) / float(np.log2(i+2))

                NDCG[str(k)] += float(now) / float(now_best)
                total_NDCG[str(k)] += 1.0

    ret = float(ret) / float(total)
    ret *= 100.0
    ret = round(ret, 4)

    for k in NDCG:
        NDCG[k] = float(NDCG[k]) / float(total_NDCG[k])
        NDCG[k] *= 100.0
        NDCG[k] = round(NDCG[k], 4)

    return ret, NDCG

def evaluate(reader, is_train_set):
    model.eval()

    metrics = {}
    metrics['CP'] = 0.0
    # metrics['NCP'] = 0.0
    metrics['ZEROS'] = 0.0
    metrics['loss'] = 0.0

    first_indices = LongTensor(range(0, hyper_params['batch_size'], 2))
    second_indices = LongTensor(range(1, hyper_params['batch_size'], 2))

    correct = 0
    not_correct = 0
    zeros = 0
    total = 0
    batch = 0

    for x, y in reader.iter():
        batch += 1
        if batch > hyper_params['testing_batch_limit']: break

        output = model(x)

        metrics['loss'] += criterion(output, y).data

        output = output[:,0]
        output_first = output[first_indices]
        output_second = output[second_indices]

        y_first = y[first_indices]
        y_second = y[second_indices]

        temp_correct = 0
        temp_not_correct = 0
        temp_zeros = 0

        temp_correct += int(torch.sum(torch.lt(y_first, y_second) * torch.lt(output_first, output_second)))
        temp_correct += int(torch.sum(torch.gt(y_first, y_second) * torch.gt(output_first, output_second)))

        temp_not_correct += int(torch.sum(torch.lt(y_first, y_second) * torch.lt(output_second, output_first)))
        temp_not_correct += int(torch.sum(torch.gt(y_first, y_second) * torch.gt(output_second, output_first)))

        temp_zeros += int(torch.sum(torch.eq(output_second, output_first)))

        correct += temp_correct
        not_correct += temp_not_correct
        zeros += temp_zeros

        assert temp_correct + temp_not_correct + temp_zeros == int(y_first.shape[0])

        total += int(y_first.shape[0])

    assert correct + not_correct + zeros == total

    metrics['CP'] = float(correct) / float(total)
    metrics['CP'] *= 100.0
    metrics['CP'] = round(metrics['CP'], 4)

    # metrics['NCP'] = float(not_correct) / float(total)
    # metrics['NCP'] *= 100.0
    # metrics['NCP'] = round(metrics['NCP'], 4)

    metrics['ZEROS'] = float(zeros) / float(total)
    metrics['ZEROS'] *= 100.0
    metrics['ZEROS'] = round(metrics['ZEROS'], 4)

    metrics['loss'] = float(metrics['loss'][0]) / float(batch)
    metrics['loss'] = round(metrics['loss'], 4)

    metrics['inversions'], ndcg = evaluate_inversions(reader, is_train_set)

    for k in ndcg:
        metrics['NDCG@' + str(k)] = ndcg[k]

    return metrics

def train(reader):
    model.train()
    total_loss = 0
    start_time = time.time()
    batch = 0
    batch_limit = int(min(train_reader.num_b, hyper_params['training_batch_limit']))

    for x, y in reader.iter():
        batch += 1
        if batch > hyper_params['training_batch_limit']: return
        
        model.zero_grad()
        optimizer.zero_grad()

        output = model(x)
        
        loss = criterion(output, y)
        loss.backward()

        torch.nn.utils.clip_grad_norm(model.parameters(), hyper_params['exploding_clip'])
        optimizer.step()

        total_loss += loss.data

        if (batch % hyper_params['batch_log_interval'] == 0 and batch > 0) or batch == batch_limit:
            div = hyper_params['batch_log_interval']
            if batch == batch_limit: div = batch_limit % hyper_params['batch_log_interval']

            cur_loss = (total_loss[0] / div)
            elapsed = time.time() - start_time

            ss = '| epoch {:3d} | {:5d}/{:5d} batches | ms/batch {:5.2f} | loss {:5.4f}'.format(
                epoch, batch, batch_limit,
                elapsed * 1000 / div, cur_loss)
            
            file_write(hyper_params['log_file'], ss)

            total_loss = 0
            start_time = time.time()

train_reader, test_reader, total_users, total_items = load_data(hyper_params)
hyper_params['total_users'] = total_users
hyper_params['total_items'] = total_items

file_write(hyper_params['log_file'], "\n\nSimulation run on: " + str(dt.datetime.now()) + "\n\n")
file_write(hyper_params['log_file'], "Data reading complete!")
file_write(hyper_params['log_file'], "Number of train batches: {:4d}".format(train_reader.num_b))
file_write(hyper_params['log_file'], "Number of test batches: {:4d}".format(test_reader.num_b))
file_write(hyper_params['log_file'], "Total Users: " + str(total_users))
file_write(hyper_params['log_file'], "Total Items: " + str(total_items) + "\n")

model = Model(hyper_params)
if is_cuda_available: model.cuda()

criterion = HingeLoss(
    hyper_params['batch_size'], hyper_params['loss_type'], hyper_params['m_loss']
)
# criterion = nn.MSELoss()
# criterion = nn.CrossEntropyLoss(size_average=True)

if hyper_params['optimizer'] == 'adagrad':
    optimizer = torch.optim.Adagrad(model.parameters(), lr=hyper_params['learning_rate'], weight_decay=hyper_params['weight_decay'])
elif hyper_params['optimizer'] == 'adadelta':
    optimizer = torch.optim.Adadelta(model.parameters(), weight_decay=hyper_params['weight_decay'])
elif hyper_params['optimizer'] == 'adam':
    optimizer = torch.optim.Adam(model.parameters(), lr=hyper_params['learning_rate'], weight_decay=hyper_params['weight_decay'])


file_write(hyper_params['log_file'], str(model))
file_write(hyper_params['log_file'], "\nModel Built!\nStarting Training...\n")

if hyper_params['data_parallel']: 
    model = nn.DataParallel(model, dim=0)
    if is_cuda_available: model = model.cuda()

best_val_hr = None

try:
    for epoch in range(1, hyper_params['epochs'] + 1):
        epoch_start_time = time.time()
        train(train_reader)
        
        metrics = evaluate(train_reader, True)
        
        string = ""
        for m in metrics:
            string += " | " + m + ' = ' + str(metrics[m])
        string += ' (TRAIN)'

        metrics = evaluate(test_reader, False)
        
        string2 = ""
        for m in metrics:
            string2 += " | " + m + ' = ' + str(metrics[m])
        string2 += ' (TEST)'

        ss  = '-' * 89
        ss += '\n| end of epoch {:3d} | time: {:5.2f}s'.format(epoch, (time.time() - epoch_start_time))
        ss += string
        ss += '\n'
        ss += '-' * 89
        ss += '\n| end of epoch {:3d} | time: {:5.2f}s'.format(epoch, (time.time() - epoch_start_time))
        ss += string2
        ss += '\n'
        ss += '-' * 89
        file_write(hyper_params['log_file'], ss)
        
        if not best_val_hr or metrics['CP'] >= best_val_hr:
            with open(hyper_params['model_file_name'], 'wb') as f: torch.save(model, f)
            best_val_hr = metrics['CP']

except KeyboardInterrupt: print('Exiting from training early')

with open(hyper_params['model_file_name'], 'rb') as f: model = torch.load(f)
metrics = evaluate(test_reader, False)

string = ""
for m in metrics:
    string += " | " + m + ' = ' + str(metrics[m])

ss  = '=' * 89
ss += '\n| End of training'
ss += string
ss += '\n'
ss += '=' * 89
file_write(hyper_params['log_file'], ss)