import numpy as np
import torch
from torch.autograd import Variable
from tqdm import tqdm

from utils import *

'''
Used to pad/clip according to the history size

to_scatter  : array to be scattered
is_user     : boolean whether to be scattered history is user history or not
is_int      : boolean whether elements to be scattered are integers or not
hyper_params: object having all hyper parameters

returns: the padded/clipped array
'''
def scatter(to_scatter):
    max_rows = max(list(map(len, to_scatter)))
    # max_rows = min(max_rows, hyper_params['max_hist_size'])
    max_rows = max(max_rows, 1) # need atleast one

    scattered = [[0] * max_rows] * len(to_scatter)
    
    now = 0
    for i in to_scatter:
        now2 = 0
        for j in i:
            scattered[now][now2] = int(j)
            now2 += 1
            if now2 >= max_rows: break # CAN USE A HYPER-PARAM TO CLIP
        now += 1
        # scattered[now] = int(i)
    
    return scattered

'''
Used to find item history i.e users that have watched this movie before a given time

item     : item id whose history needs to be found
time     : before what time is the history needed
item_hist: object having all timestamped item histories

returns: item history ; rating of the history ; timestamps of the history
'''
def find_i_hist(item, time, item_hist):
    hist = []
    hist_rating = []
    hist_time = []
    time = float(time)

    for i in item_hist[str(item)]:
        if float(i[2]) < time:
            hist.append(i[0])
            hist_rating.append(float(i[1]) / float(5))
            hist_time.append(i[2])
        else:
            if len(hist) == 0: hist.append(0)
            return hist
    if len(hist) == 0: hist.append(0)
    return hist

'''
Main function called in the main.py file

hyper_params: object having all hyper parameters

returns: object of class DataReader for train data ; test array ; number of users ; number of items
'''
def load_data(hyper_params):
    
    file_write(hyper_params['log_file'], "Started reading data file")
    
    train = load_obj_json(hyper_params['data_base'] + 'train_' + hyper_params['project_name'])
    test = load_obj_json(hyper_params['data_base'] + 'test_' + hyper_params['project_name'])
    user_hist = load_obj_json(hyper_params['data_base'] + 'user_hist_' + hyper_params['project_name'])
    item_hist = load_obj_json(hyper_params['data_base'] + 'item_hist_' + hyper_params['project_name'])
    bad_set = load_obj_json(hyper_params['data_base'] + 'bad_set_' + hyper_params['project_name'])
    good_set = load_obj_json(hyper_params['data_base'] + 'good_set_' + hyper_params['project_name'])

    file_write(hyper_params['log_file'], "Data Files loaded!")

    train_reader = DataReader(hyper_params, train, len(user_hist), item_hist, bad_set, good_set, True)
    test_reader = DataReader(hyper_params, test, len(user_hist), item_hist, bad_set, good_set, False)

    return train_reader, test_reader, len(user_hist), len(item_hist)

'''
Class which manages train and test data
Has a function iter which yields the x_batch, y_batch

hyper_params: object having all hyper parameters
data        : training data
num_users   : total number of users
item_hist   : object having all item history (containing users)
'''
class DataReader:

    def __init__(self, hyper_params, data, num_users, item_hist, bad_set, good_set, is_training):
        self.hyper_params = hyper_params
        self.batch_size = hyper_params['batch_size']
        self.item_hist = item_hist
        self.num_users = num_users
        self.num_items = len(item_hist)
        self.data = data
        self.bad_set = bad_set
        self.good_set = good_set
        self.is_training = is_training
        self.all_users = []

        self.number_users()
        self.number()

        self._x_batches = []
        self._y_batches = []

    '''
    Counts the number of batches
    '''
    def number(self):
        users_done = 0
        count = 0
        y_batch = []

        for user in self.data:

            if users_done > self.hyper_params['number_users_to_keep']: break
            users_done += 1

            for i in range(1, len(self.data[user])):
                for ii in range(i+1, len(self.data[user])):

                    if self.data[user][i][1] == self.data[user][ii][1]: continue

                    y_batch.append(0)
                    y_batch.append(0)

                    if len(y_batch) == self.batch_size:
                        y_batch = []
                        count += 1

        self.num_b = count

    '''
    Counts the number of batches
    '''
    def number_users(self):
        users_done = 0
        count = 0

        for user in self.data:

            if users_done > self.hyper_params['number_users_to_keep']: break
            users_done += 1

            y_batch = []

            for i in range(1, len(self.data[user])):
                for ii in range(i+1, len(self.data[user])):

                    if self.data[user][i][1] == self.data[user][ii][1]: continue

                    y_batch.append(0)
                    y_batch.append(0)

            if len(y_batch) > 0:
                count += 1
                self.all_users.append(user)

        self.num_u = count

    '''
    Yields x_batch and y_batch at each time step

    returns: [
        x_batch_user, (users)
        x_batch_item (items)
    ], y_batch (rating given by x_batch_user to x_batch_item)
    '''
    def iter(self):
        users_done = 0

        x_batch_user = []
        x_batch_user_hist = []
        x_batch_item = []
        x_batch_item_hist = []
        y_batch = []

        for user in self.data:

            if users_done > self.hyper_params['number_users_to_keep']: break
            users_done += 1

            for i in range(1, len(self.data[user])):
                for ii in range(i+1, len(self.data[user])):

                    if self.data[user][i][1] == self.data[user][ii][1]: continue

                    first, second = i, ii
                    if self.data[user][i][1] < self.data[user][ii][1]: first, second = second, first

                    x_batch_user.append(int(user))
                    x_batch_user.append(int(user))

                    x_batch_user_hist.append([ i[0] for i in self.data[user][:first] ])
                    x_batch_user_hist.append([ i[0] for i in self.data[user][:second] ])

                    x_batch_item.append(self.data[user][first][0])
                    x_batch_item.append(self.data[user][second][0])

                    x_batch_item_hist.append(find_i_hist(self.data[user][first][0], self.data[user][first][2], self.item_hist))
                    x_batch_item_hist.append(find_i_hist(self.data[user][second][0], self.data[user][second][2], self.item_hist))

                    y_batch.append(float(self.data[user][first][1]) / float(5))
                    y_batch.append(float(self.data[user][second][1]) / float(5))

                    if len(y_batch) == self.batch_size:

                        yield [
                            Variable(LongTensor(x_batch_user)), 
                            [ Variable(LongTensor(scatter(x_batch_user_hist))), list(map(len, x_batch_user_hist)) ], 
                            Variable(LongTensor(x_batch_item)),
                            [ Variable(LongTensor(scatter(x_batch_item_hist))), list(map(len, x_batch_item_hist)) ]
                        ], Variable(FloatTensor(y_batch))
                        
                        x_batch_user = []
                        x_batch_user_hist = []
                        x_batch_item = []
                        x_batch_item_hist = []
                        y_batch = []

    def iter_eval(self, is_train_set):

        temp3 = len(self.all_users)
        if is_train_set == True: temp3 = min(temp3, self.hyper_params['user_limit_inversion'])
        temp3 = min(temp3, self.hyper_params['number_users_to_keep'])

        for jj in tqdm(range(temp3)):
            user = self.all_users[jj]

            x_batch_user = []
            x_batch_user_hist = []
            x_batch_item = []
            x_batch_item_hist = []
            y_batch = []
            all_movies = []

            for i in range(1, len(self.data[user])):
                all_movies.append(self.data[user][i][0])

                for ii in range(i+1, len(self.data[user])):

                    if self.data[user][i][1] == self.data[user][ii][1]: continue

                    first, second = i, ii
                    if self.data[user][i][1] < self.data[user][ii][1]: first, second = second, first

                    x_batch_user.append(int(user))
                    x_batch_user.append(int(user))

                    x_batch_user_hist.append([ i[0] for i in self.data[user][:first] ])
                    x_batch_user_hist.append([ i[0] for i in self.data[user][:second] ])

                    x_batch_item.append(self.data[user][first][0])
                    x_batch_item.append(self.data[user][second][0])

                    x_batch_item_hist.append(find_i_hist(self.data[user][first][0], self.data[user][first][2], self.item_hist))
                    x_batch_item_hist.append(find_i_hist(self.data[user][second][0], self.data[user][second][2], self.item_hist))

                    y_batch.append(float(self.data[user][first][1]) / float(5))
                    y_batch.append(float(self.data[user][second][1]) / float(5))

            if len(y_batch) > 0:
                yield [
                    Variable(LongTensor(x_batch_user)), 
                    [ Variable(LongTensor(scatter(x_batch_user_hist))), list(map(len, x_batch_user_hist)) ], 
                    Variable(LongTensor(x_batch_item)),
                    [ Variable(LongTensor(scatter(x_batch_item_hist))), list(map(len, x_batch_item_hist)) ]
                ], Variable(FloatTensor(y_batch)), all_movies
