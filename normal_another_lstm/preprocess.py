import os
import sys
reload(sys)
sys.setdefaultencoding("utf8")

from hyper_params import *
from utils import *

train = {}
val = {}
test = {}

f = open(hyper_params['data_base'] + 'ratings.dat')
lines = f.readlines()

max_time, min_time = 0, 1000000000000000000

for line in lines:
	temp = map(int, line.strip().split("::"))

	max_time = max(max_time, int(temp[3]))
	min_time = min(min_time, int(temp[3]))

user_hist = {}
item_hist = {}

for line in lines:
	temp = map(int, line.strip().split("::"))

	if temp[0] not in user_hist: user_hist[temp[0]] = []
	user_hist[temp[0]].append([temp[1], temp[2], float(temp[3] - min_time) / float(max_time - min_time)])

	if temp[1] not in item_hist: item_hist[temp[1]] = []
	item_hist[temp[1]].append([temp[0], temp[2], float(temp[3] - min_time) / float(max_time - min_time)])

f = open(hyper_params['data_base'] + 'movies.dat')
lines = f.readlines()
max_item_no = 0

for line in lines:
	temp = line.strip().split("::")
	max_item_no = max(max_item_no, int(temp[0]))

for i in range(max_item_no):
	if i not in item_hist: item_hist[i] = []

for user in user_hist:
	user_hist[user].sort(key=lambda x: x[2])

for item in item_hist:
	item_hist[item].sort(key=lambda x: x[2])

split = hyper_params['data_split']

for user in user_hist:

	train_split = user_hist[user][:int(split[0] * len(user_hist[user]))]
	val_split = user_hist[user][int(split[0] * len(user_hist[user])) : int((split[0] + split[1]) * len(user_hist[user]))]
	test_split = user_hist[user][int((split[0] + split[1]) * len(user_hist[user])):]

	train[user] = train_split
	val[user] = val_split
	test[user] = test_split

save_obj_json(train, hyper_params['data_base'] + "train_normal_another_lstm")
save_obj_json(val, hyper_params['data_base'] + "val_normal_another_lstm")
save_obj_json(test, hyper_params['data_base'] + "test_normal_another_lstm")
save_obj_json(item_hist, hyper_params['data_base'] + "item_hist_normal_another_lstm")
save_obj_json(user_hist, hyper_params['data_base'] + "user_hist_normal_another_lstm")
