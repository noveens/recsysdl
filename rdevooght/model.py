import math
import torch
import random
import numpy as np
from torch import nn
from torch.autograd import Variable
import torch.nn.functional as F

class Model(nn.Module):
    def __init__(self, hyper_params):
        super(Model, self).__init__()
        self.hyper_params = hyper_params

        self.embed = nn.Embedding(hyper_params['total_items'] + 1, hyper_params['embed_size'])

        self.rnn = nn.GRU(hyper_params['embed_size'], hyper_params['rnn_size'], hyper_params['rnn_layers'], dropout=hyper_params['rnn_dropout'], bidirectional=hyper_params['rnn_bi'], batch_first=True)

        if hyper_params['rnn_attention'] == True:
            # self.attention_weights = nn.Parameter(torch.FloatTensor(hyper_params['item_rnn_size']), requires_grad = True)
            self.attention_dense_layer = nn.Linear(hyper_params['rnn_size'], 1)
            self.attention_dropout = nn.Dropout(0.2)
            # self.attention_activation = nn.Tanh()
            self.attention_activation = nn.ReLU()
            self.softmax = nn.Softmax(1)

        self.activation = nn.ReLU()

        self.out_layer = nn.Linear(hyper_params['rnn_size'], hyper_params['total_items'] + 1)
        self.out_drop = nn.Dropout(hyper_params['end_dropout'])

    def init_hidden(self, batch_size, bi, layers, size):
        mul = 1
        if bi == True: mul = 2
        return Variable(torch.zeros(mul * layers, batch_size, size).cuda(), requires_grad=True)
        # return Variable(torch.randn(mul * layers, batch_size, size).cuda(), requires_grad=True)

    def repack(self, h):
        if type(h) == Variable:
            return Variable(h.data.cuda(), requires_grad=True)
        else:
            return tuple(repackage_hidden(v) for v in h)

    def attention_parameter(self, hidden_states):
        scores = self.attention_activation(hidden_states.matmul(self.attention_weights))
        scores = self.softmax(scores)
        # print(scores)
        # print(torch.sum(scores, 1))
        weighted = torch.mul(hidden_states, scores.unsqueeze(-1).expand_as(hidden_states))
        # print(weighted.sum(1).squeeze())
        # print("\n\n")
        return weighted.sum(1).squeeze()

    def attention(self, hidden_states):
        shape = hidden_states.shape
        scores = self.attention_dropout(self.attention_activation(self.attention_dense_layer(hidden_states.contiguous().view(-1, shape[-1])).view(-1, shape[1])))
        scores = self.softmax(scores)
        # print(scores)
        # print(torch.sum(scores, 1))
        return scores.unsqueeze(1).bmm(hidden_states).squeeze(1)
        # print(final)
        # print("\n\n")

    def forward(self, src):

        src = self.embed(src)               # [bsz x item_embed_size]
        
        # self.rnn_hidden = self.repack(self.rnn_hidden)
        self.rnn_hidden = self.init_hidden(src.size(0), self.hyper_params['rnn_bi'], self.hyper_params['rnn_layers'], self.hyper_params['rnn_size'])
        self.rnn.flatten_parameters()
        rnn_output, self.rnn_hidden = self.rnn(src, self.rnn_hidden) # [bsz x rnn_size]

        # if self.hyper_params['rnn_attention'] == True: attention_output = self.attention(rnn_output)
        # else: attention_output = rnn_output[:, -1, :]
        
        if self.hyper_params['bptt'] == False: attention_output = rnn_output[:, -1, :]
        else: attention_output = rnn_output

        output = self.out_drop(self.out_layer(attention_output))
        # output = self.activation(output)

        return output # [bsz x total_items]
