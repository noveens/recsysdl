import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable

import numpy as np
import time
import pickle
import json
import gc
import datetime as dt
import sys

from data import load_data
from utils import *

def eval_acc(reader, batch_size):
    model.eval()

    corr = 0
    total = 0
    batch = 0
    next_print = 0.1
    jump = 0.1

    for x, y in reader.iter():
        output = model(x)
        y = y.data

        _, top = torch.sort(output)
        top_k = top[:,-1].data
        matched = torch.nonzero(torch.eq(top_k, y))

        if len(matched.size()) == 2: corr += matched.size(0)
        total += batch_size
        batch += 1

        if float(batch) / float(reader.num_b) > next_print:
            print("done " + str(next_print * 100.0) + "%")
            next_print += jump

    return (float(corr) / float(total)) * 100.0

def eval_rmse(reader, batch_size):
    model.eval()

    mse = 0.0
    total = 0
    batch = 0
    next_print = 0.1
    jump = 0.1

    for x, y in reader.iter():
        output = model(x)
        y = y.data

        _, top = torch.sort(output)
        top_k = top[:,-1].data

        mse += torch.sum(torch.pow((top_k - y).float(), 2))

        total += batch_size
        batch += 1

        if float(batch) / float(reader.num_b) > next_print:
            print("done " + str(next_print * 100.0) + "%")
            next_print += jump

    return (float(mse) / float(total)) ** 0.5

with open(sys.argv[1], 'rb') as f: model = torch.load(f)
hyper_params = model.hyper_params
train_reader, val_reader, test_reader, total_users, total_items = load_data(hyper_params)

acc = eval_rmse(test_reader, hyper_params['batch_size'])
file_write(hyper_params['log_file'], "\nWhile Evaluation, RMSE = " + str(acc) + "\n")

# acc = eval_acc(test_reader, hyper_params['batch_size'])
# file_write(hyper_params['log_file'], "\nWhile Evaluation, Accuracy = " + str(acc) + "\n")