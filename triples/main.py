import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable

import numpy as np
import time
import random
import pickle
import json
import gc
import datetime as dt
from tqdm import tqdm

from data import load_data
from model import Model
from utils import *
from hyper_params import *

def evaluate(test):
    model.eval()

    metrics = {}
    metrics['HR@1'] = 0.0
    metrics['HR@5'] = 0.0
    metrics['HR@10'] = 0.0
    metrics['HR@50'] = 0.0

    for i in tqdm(range(min(len(test), hyper_params['testing_batch_limit']))):
        user_batch = test[i]

        output = model([
            Variable(torch.cuda.LongTensor(user_batch[0])),
            Variable(torch.cuda.FloatTensor(user_batch[1])), 
            Variable(torch.cuda.FloatTensor(user_batch[2])),
            
            Variable(torch.cuda.LongTensor(user_batch[3])),
            Variable(torch.cuda.FloatTensor(user_batch[4])), 
            Variable(torch.cuda.FloatTensor(user_batch[5])),
            
            Variable(torch.cuda.LongTensor(user_batch[6])), 
            Variable(torch.cuda.LongTensor(user_batch[7]))
        ])

        print(output.shape)
        _, top = torch.sort(output[:, 0], descending=True)

        for k in [1, 5, 10, 50]:
            string = 'HR@' + str(k)
            tt = top[:k].data
            # print(tt)
            if 0 in tt:
                metrics[string] += 1.0

    for k in [1, 5, 10, 50]:
        string = 'HR@' + str(k)
        metrics[string] /= float(min(len(test), hyper_params['testing_batch_limit']))
        metrics[string] *= 100.0
        metrics[string] = round(metrics[string], 4)

    return metrics

def evaluate_cp(test):
    model.eval()

    metrics = {}
    metrics['HR@1'] = 0.0
    metrics['HR@5'] = 0.0
    metrics['HR@10'] = 0.0
    metrics['HR@50'] = 0.0

    for i in tqdm(range(min(len(test), hyper_params['testing_batch_limit']))):
        user_batch = test[i]

        output = model([
            Variable(torch.cuda.LongTensor(user_batch[0])),
            Variable(torch.cuda.FloatTensor(user_batch[1])), 
            Variable(torch.cuda.FloatTensor(user_batch[2])),
            
            Variable(torch.cuda.LongTensor(user_batch[3])),
            Variable(torch.cuda.FloatTensor(user_batch[4])), 
            Variable(torch.cuda.FloatTensor(user_batch[5])),
            
            Variable(torch.cuda.LongTensor(user_batch[6])), 
            Variable(torch.cuda.LongTensor(user_batch[7]))
        ])

        print(output.shape)
        _, top = torch.sort(output[:, 0], descending=True)

        for k in [1, 5, 10, 50]:
            string = 'HR@' + str(k)
            tt = top[:k].data
            # print(tt)
            if 0 in tt:
                metrics[string] += 1.0

    for k in [1, 5, 10, 50]:
        string = 'HR@' + str(k)
        metrics[string] /= float(min(len(test), hyper_params['testing_batch_limit']))
        metrics[string] *= 100.0
        metrics[string] = round(metrics[string], 4)

    return metrics

def train(reader):
    model.train()
    total_loss = 0
    start_time = time.time()
    batch = 0

    for x, y in reader.iter():
        batch += 1
        if batch > hyper_params['training_batch_limit']: return
        
        optimizer.zero_grad()

        output = model(x)

        # print(y)
        # print(output)
        # print("\n")
        
        loss = criterion(output, y)
        loss.backward()

        torch.nn.utils.clip_grad_norm(model.parameters(), hyper_params['exploding_clip'])
        optimizer.step()

        total_loss += loss.data

        if batch % hyper_params['batch_log_interval'] == 0 and batch > 0:
            cur_loss = (total_loss[0] / hyper_params['batch_log_interval'])
            elapsed = time.time() - start_time

            ss = '| epoch {:3d} | {:5d}/{:5d} batches | ms/batch {:5.2f} | loss {:5.4f}'.format(
                epoch, batch, int(min(train_reader.num_b, hyper_params['training_batch_limit'])),
                elapsed * 1000 / hyper_params['batch_log_interval'], cur_loss)
            
            file_write(hyper_params['log_file'], ss)

            total_loss = 0
            start_time = time.time()

train_reader, test, total_users, total_items = load_data(hyper_params)
hyper_params['total_users'] = total_users
hyper_params['total_items'] = total_items

file_write(hyper_params['log_file'], "\n\nSimulation run on: " + str(dt.datetime.now()) + "\n\n")
file_write(hyper_params['log_file'], "Data reading complete!")
file_write(hyper_params['log_file'], "Number of train batches: {:4d}".format(train_reader.num_b))
file_write(hyper_params['log_file'], "Number of test batches: {:4d}".format(len(test)))
file_write(hyper_params['log_file'], "Total Users: " + str(total_users))
file_write(hyper_params['log_file'], "Total Items: " + str(total_items) + "\n")

model = Model(hyper_params)
model.cuda()

class HingeLoss(torch.nn.Module):
    def __init__(self, batch_size):
        super(HingeLoss,self).__init__()
        self.first_indices = torch.cuda.LongTensor(range(0, batch_size, 2))
        self.second_indices = torch.cuda.LongTensor(range(1, batch_size, 2))
        self.ones_while_min = Variable(torch.ones(int(batch_size / 2)).cuda())

    def forward(self, output, y):
        output_first = output[self.first_indices][:, 0]
        output_second = output[self.second_indices][:, 0]

        y_first = y[self.first_indices]
        y_second = y[self.second_indices]

        is_same_y = torch.eq(y_first, y_second).float()
        is_same_output = torch.lt(torch.abs(output_first - output_second), 0.001).float()
        is_same = torch.min(is_same_y + is_same_output, self.ones_while_min)
        
        first_part = (y_first - y_second) * (output_second - output_first)
        second_part = is_same * torch.pow(output_first - y_first, 2)
        third_part = is_same * torch.pow(output_second - y_second, 2)
        summed = first_part + second_part + third_part
        ############## CAN GIVE WEIGHTS TO PARTS

        return torch.sum(summed)

criterion = HingeLoss(hyper_params['batch_size'])
# criterion = nn.MSELoss()
# criterion = nn.CrossEntropyLoss(size_average=True)

# optimizer = torch.optim.Adagrad(model.parameters(), lr=hyper_params['learning_rate'])
optimizer = torch.optim.Adadelta(model.parameters())

file_write(hyper_params['log_file'], str(model))
file_write(hyper_params['log_file'], "\nModel Built!\nStarting Training...\n")

if hyper_params['data_parallel']: model = nn.DataParallel(model, dim=0).cuda()

best_val_hr = None

try:
    for epoch in range(1, hyper_params['epochs'] + 1):
        epoch_start_time = time.time()
        train(train_reader)
        metrics = evaluate(test)
        
        string = ""
        for m in metrics:
            string += " | " + m + ' = ' + str(metrics[m])

        ss  = '-' * 89
        ss += '\n| end of epoch {:3d} | time: {:5.2f}s'.format(epoch, (time.time() - epoch_start_time))
        ss += string
        ss += '\n'
        ss += '-' * 89
        file_write(hyper_params['log_file'], ss)
        
        if not best_val_hr or metrics['HR@10'] >= best_val_hr:
            with open(hyper_params['model_file_name'], 'wb') as f: torch.save(model, f)
            best_val_hr = metrics['HR@10']

except KeyboardInterrupt: print('Exiting from training early')

with open(hyper_params['model_file_name'], 'rb') as f: model = torch.load(f)
metrics = evaluate(test)

string = ""
for m in metrics:
    string += " | " + m + ' = ' + str(metrics[m])

ss  = '=' * 89
ss += '\n| End of training'
ss += string
ss += '\n'
ss += '=' * 89
file_write(hyper_params['log_file'], ss)