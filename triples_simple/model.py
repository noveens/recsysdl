import math
import torch
import random
import numpy as np
from torch import nn
from torch.autograd import Variable
import torch.nn.functional as F

class Model(nn.Module):
    def __init__(self, hyper_params):
        super(Model, self).__init__()
        self.hyper_params = hyper_params

        self.user_embed = nn.Embedding(hyper_params['total_users'] + 1, hyper_params['user_embed_size'])
        self.item_embed = nn.Embedding(hyper_params['total_items'] + 1, hyper_params['item_embed_size'])

        # self.activation = nn.ReLU()
        if hyper_params['activation_last'] == 'sigmoid':
            self.activation_last = nn.Sigmoid()
        elif hyper_params['activation_last'] == 'tanh':
            self.activation_last = nn.Tanh()
        elif hyper_params['activation_last'] == 'relu':
            self.activation_last = nn.ReLU()

        # self.dropout = nn.Dropout(0.2)
        self.dropout = nn.Dropout(hyper_params['dropout'])

        self.batch_norm1 = nn.BatchNorm1d(hyper_params['user_embed_size'] + hyper_params['item_embed_size'])

        # self.batch_norm1 = nn.BatchNorm1d(64)
        # self.batch_norm2 = nn.BatchNorm1d(16)
        # self.batch_norm3 = nn.BatchNorm1d(4)

        prev = hyper_params['user_embed_size'] + hyper_params['item_embed_size']
        # self.layer1 = nn.Linear(prev, 64)
        # self.layer2 = nn.Linear(64, 16)
        # self.layer3 = nn.Linear(16, 4)
        # self.layer4 = nn.Linear(4, 1)
        self.layer1 = nn.Linear(prev, 1)
        # self.layer2 = nn.Linear(32, 1)

    def forward(self, src):

        user = self.user_embed(src[0])               # [bsz x user_embed_size]
        if self.hyper_params['embed_dropout'] == True: user = self.dropout(user)

        item = self.item_embed(src[1])               # [bsz x item_embed_size]
        if self.hyper_params['embed_dropout'] == True: item = self.dropout(item)
        
        output = torch.cat([user, item], dim = -1)
        output = self.batch_norm1(output)

        # print(max([ max(i) for i in output.data.cpu().numpy() ]))
        # print(min([ min(i) for i in output.data.cpu().numpy() ]))
        # print()

        # output = self.layer1(output)
        # output = self.dropout(output)
        # output = self.activation(output)
        # output = self.batch_norm1(output)

        # output = self.layer2(output)
        # output = self.dropout(output)
        # output = self.activation(output)
        # output = self.batch_norm2(output)

        # output = self.layer3(output)
        # output = self.dropout(output)
        # output = self.activation(output)
        # output = self.batch_norm3(output)

        # output = self.layer4(output)
        # output = self.dropout(output)
        # output = self.activation_last(output)
         
        output = self.layer1(output)
        output = self.dropout(output)
        output = self.activation_last(output)

        # if self.hyper_params['activation_last'] == 'tanh':
        #     output = output + 1.0
        #     output = output / 2.0

        return output # [bsz x last_layer_dim]

class Model2(nn.Module):
    def __init__(self, hyper_params):
        super(Model, self).__init__()
        self.hyper_params = hyper_params

        self.user_embed = nn.Embedding(hyper_params['total_users'] + 1, hyper_params['user_embed_size'])
        self.item_embed = nn.Embedding(hyper_params['total_items'] + 1, hyper_params['item_embed_size'])

    def forward(self, src):

        user = self.user_embed(src[0])  # [bsz x user_embed_size]
        item = self.item_embed(src[1])  # [bsz x item_embed_size]

        return torch.sum(user * item, -1)
