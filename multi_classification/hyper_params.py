hyper_params = {
    'data_base': '../saved_data/',
    'model_file_name': '',
    'log_file': '',
    # 'data_split': [0.7, 0.1, 0.2], # Train : Val : Test
    'max_user_hist': 500,
    'min_user_hist': 0,
    'no_negs': 99, # Needs to be odd
    'window_size': 5,

    'learning_rate': 0.5, # Not Using this, using default
    'epochs': 20,
    'batch_size': 32, # Needs to be even
    'main_time_steps': 5,
    'bptt': True,
    'difference_thresh': 0.001,

    'user_embed_size': 50,
    'item_embed_size': 50,

    'user_rnn_size': 32,
    'user_rnn_layers': 1,
    'user_rnn_dropout': 0.1,
    'user_rnn_bi': False,
    'user_hist_size': 100,
    'user_attention': True,

    'item_rnn_size': 32,
    'item_rnn_layers': 1,
    'item_rnn_dropout': 0.1,
    'item_rnn_bi': False,
    'item_hist_size': 100,
    'item_attention': True,

    'exploding_clip': 0.25,
    'training_batch_limit': 1000000,
    'testing_batch_limit': 1000000,
    'batch_log_interval': 1000,

    'is_cuda': True,
    'data_parallel': False,
}

file_name  = '_lr_' + str(hyper_params['learning_rate'])
file_name += '_user_rnn_size_' + str(hyper_params['user_rnn_size'])
file_name += '_item_rnn_size_' + str(hyper_params['item_rnn_size'])
file_name += '_user_embed_size_' + str(hyper_params['user_embed_size'])
file_name += '_item_embed_size_' + str(hyper_params['item_embed_size'])
file_name += '_user_rnn_layers_' + str(hyper_params['user_rnn_layers'])
file_name += '_item_rnn_layers_' + str(hyper_params['item_rnn_layers'])
file_name += '_user_hist_size_' + str(hyper_params['user_hist_size'])
file_name += '_item_hist_size_' + str(hyper_params['item_hist_size'])
file_name += '_user_attention_' + str(hyper_params['user_attention'])
file_name += '_item_attention_' + str(hyper_params['item_attention'])

hyper_params['log_file'] = '../saved_logs/classification_log' + file_name + '.txt'
hyper_params['model_file_name'] = '../saved_models/classification_model' + file_name + '.pt'