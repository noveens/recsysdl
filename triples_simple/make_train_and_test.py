from utils import *
from hyper_params import *

train = load_obj_json(hyper_params['data_base'] + 'train_triples_simple')
test = load_obj_json(hyper_params['data_base'] + 'test_triples_simple')
user_hist = load_obj_json(hyper_params['data_base'] + 'user_hist_triples_simple')
item_hist = load_obj_json(hyper_params['data_base'] + 'item_hist_triples_simple')
bad_set = load_obj_json(hyper_params['data_base'] + 'bad_set_triples_simple')
good_set = load_obj_json(hyper_params['data_base'] + 'good_set_triples_simple')

train_save = []
test_save = []

users_done = 0
for user in train:

    if users_done > hyper_params['number_users_to_keep']: break
    users_done += 1

    for i in range(len(train[user])):
        for ii in range(i+1, len(train[user])):

            if train[user][i][1] == train[user][ii][1]: continue

            item_1 = train[user][i][0]
            item_2 = train[user][ii][0]
            r1 = float(train[user][i][1]) / float(5)
            r2 = float(train[user][ii][1]) / float(5)

            if train[user][i][1] < train[user][ii][1]:
                temp = item_1
                item_1 = item_2
                item_2 = temp

                temp2 = r1
                r1 = r2
                r2 = temp2

            train_save.append([int(user), int(item_1), int(item_2), r1, r2])

    for i in range(len(good_set[user])):
        for ii in range(len(bad_set[user])):

        	train_save.append([int(user), int(good_set[user][i]), int(bad_set[user][ii]), 2.0, -1.0])

users_done = 0
for user in test:

    if users_done > hyper_params['number_users_to_keep']: break
    users_done += 1

    for i in range(len(test[user])):
        for ii in range(i+1, len(test[user])):

            if test[user][i][1] == test[user][ii][1]: continue

            item_1 = test[user][i][0]
            item_2 = test[user][ii][0]
            r1 = float(test[user][i][1]) / float(5)
            r2 = float(test[user][ii][1]) / float(5)

            if test[user][i][1] < test[user][ii][1]:
                temp = item_1
                item_1 = item_2
                item_2 = temp

                temp2 = r1
                r1 = r2
                r2 = temp2

            test_save.append([int(user), int(item_1), int(item_2), r1, r2])

import numpy
a = numpy.asarray(train_save)
numpy.savetxt("train.csv", a, delimiter=",")

b = numpy.asarray(test_save)
numpy.savetxt("test.csv", b, delimiter=",")