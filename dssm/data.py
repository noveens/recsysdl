import numpy as np
import torch
from torch.autograd import Variable

from utils import *

def load_data_dssm(hyper_params):
    
    file_write(hyper_params['log_file'], "Started reading data file")
    
    history = load_obj_json(hyper_params['data_base'] + 'history_dssm')
    negs = load_obj_json(hyper_params['data_base'] + 'negs_dssm')
    train = load_obj_json(hyper_params['data_base'] + 'train_dssm')
    test = load_obj_json(hyper_params['data_base'] + 'test_dssm')
    user_hist = load_obj_json(hyper_params['data_base'] + 'user_hist_dssm')
    item_hist = load_obj_json(hyper_params['data_base'] + 'item_hist_dssm')
    
    file_write(hyper_params['log_file'], "Data Files loaded!")

    train_reader = DataReaderDSSM(hyper_params, train, len(user_hist), item_hist, negs, history)
    test_reader = DataReaderDSSM(hyper_params, test, len(user_hist), item_hist, negs, history)

    return train_reader, test, negs, history, item_hist, len(user_hist), len(item_hist)

class DataReaderDSSM:

    def __init__(self, hyper_params, data, num_users, item_hist, negs, history):
        self.hyper_params = hyper_params
        self.batch_size = hyper_params['batch_size']
        self.item_hist = item_hist
        self.num_users = num_users
        self.num_items = len(item_hist)
        self.data = data
        self.negs = negs
        self.history = history

        self.number()

        self._x_batches = []
        self._y_batches = []

    def number(self):
        y_batch = []

        count = 0

        for user in self.data:            
            for item in self.data[user]:
                y_batch.append(1)

                if len(y_batch) == self.hyper_params['batch_size']:
                    count += 1
                    y_batch = []

            for item in self.negs[user]:
                y_batch.append(0)

                if len(y_batch) == self.hyper_params['batch_size']:
                    count += 1
                    y_batch = []

        self.num_b = count

    def iter(self):
        x_user_hist_batch, x_item_hist_batch, y_batch = [], [], []
        for user in self.data:
            for item in self.data[user]:
                x_user_hist_batch.append(self.history[user])
                x_item_hist_batch.append(self.find_i_hist(item))
                y_batch.append([1.0])

                if len(y_batch) == self.hyper_params['batch_size']:
                    yield Variable(torch.cuda.LongTensor(x_user_hist_batch)), Variable(torch.cuda.LongTensor(x_item_hist_batch)), Variable(torch.cuda.FloatTensor(y_batch))
                    x_user_hist_batch, x_item_hist_batch, y_batch = [], [], []

            for item in self.negs[user]:
                x_user_hist_batch.append(self.history[user])
                x_item_hist_batch.append(self.find_i_hist(item))
                y_batch.append([0.0])

                if len(y_batch) == self.hyper_params['batch_size']:
                    yield Variable(torch.cuda.LongTensor(x_user_hist_batch)), Variable(torch.cuda.LongTensor(x_item_hist_batch)), Variable(torch.cuda.FloatTensor(y_batch))
                    x_user_hist_batch, x_item_hist_batch, y_batch = [], [], []

    def find_i_hist(self, item):
        hist = []

        for i in self.item_hist[str(item)]:
            hist.append(i[0])
            if len(hist) == self.hyper_params['item_hist_size']:
                return hist

        while len(hist) < self.hyper_params['item_hist_size']:
            hist.append(0)

        return hist
