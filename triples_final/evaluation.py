import numpy as np
import functools

import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable

from utils import *

def mergeSortInversions(arr, y_pair_map):
    if len(arr) == 1:
        return arr, 0
    else:
        a = arr[:int(len(arr)/2)]
        b = arr[int(len(arr)/2):]

        a, ai = mergeSortInversions(a, y_pair_map)
        b, bi = mergeSortInversions(b, y_pair_map)
        c = []

        i = 0
        j = 0
        inversions = 0 + ai + bi

    while i < len(a) and j < len(b):
        m1 = a[i]
        m2 = b[j]

        if m2 in y_pair_map[m1] and y_pair_map[m1][m2] == 1:
            c.append(b[j])
            j += 1
            inversions += (len(a)-i)
        else:
            c.append(a[i])
            i += 1

    c += a[i:]
    c += b[j:]

    return c, inversions

def evaluate_inversions(model, criterion, reader, hyper_params, is_train_set):
    model.eval()

    ret = 0.0
    
    NDCG = {}
    total_NDCG = {}

    Ks = [1, 5, 10, 15, 20]

    for k in Ks:
        NDCG[str(k)] = 0.0
        total_NDCG[str(k)] = 0.0
    
    # NDCG['5'] = 0.0
    # NDCG['10'] = 0.0
    # NDCG['20'] = 0.0

    # total_NDCG['5'] = 0.0
    # total_NDCG['10'] = 0.0
    # total_NDCG['20'] = 0.0

    total = 0
    user_done = 0

    for x, y, all_movies in reader.iter_eval(is_train_set):
        user_done += 1
        if is_train_set == True and user_done >= hyper_params['user_limit_inversion']: break

        output = model(x)
        output = output[:,0]

        first_indices = LongTensor(range(0, x[0].shape[0], 2))
        second_indices = LongTensor(range(1, x[0].shape[0], 2))

        output_first = output[first_indices]
        output_second = output[second_indices]

        y_first = y[first_indices]
        y_second = y[second_indices]

        x_first = x[2][first_indices]
        x_second = x[2][second_indices]

        y_diff = torch.gt(y_first, y_second).float() - torch.gt(y_second, y_first).float()
        out_diff = torch.gt(output_first, output_second).float() - torch.gt(output_second, output_first).float()

        # print(y_diff)
        # print(out_diff)
        # print()

        y_pair_map = {}
        out_pair_map = {}
        y_r = {}
        for i in range(len(x_first)):

            m1 = int(x_first[i])
            m2 = int(x_second[i])

            y_r[str(m1)] = round(float(y_first[i].data), 2)
            y_r[str(m2)] = round(float(y_second[i].data), 2)

            if m1 not in y_pair_map: y_pair_map[m1] = {}
            if m2 not in y_pair_map: y_pair_map[m2] = {}
            if m1 not in out_pair_map: out_pair_map[m1] = {}
            if m2 not in out_pair_map: out_pair_map[m2] = {}

            y_pair_map[m1][m2] = int(y_diff[i])
            y_pair_map[m2][m1] = -1 * int(y_diff[i])

            out_pair_map[m1][m2] = int(out_diff[i])
            out_pair_map[m2][m1] = -1 * int(out_diff[i])

        # print(y_pair_map)
        # print(out_pair_map)
        # print()

        def compare_out(item1, item2):
            if item2 not in out_pair_map[item1]: return 0
            return out_pair_map[item1][item2]
        
        # print(all_movies)

        all_movies = sorted(all_movies, key=functools.cmp_to_key(compare_out))

        # print(all_movies)

        _, errors = mergeSortInversions(all_movies, y_pair_map)
        total2 = (len(all_movies) * (len(all_movies) - 1)) / 2
        
        # errors2 = 0
        # for i in range(len(all_movies)):
        #     for j in range(i+1, len(all_movies)):
        #         m1 = all_movies[i]
        #         m2 = all_movies[j]
        #         if m2 in y_pair_map[m1] and y_pair_map[m1][m2] == 1: errors2 += 1
        #         total2 += 1

        # assert errors == errors2

        # print(errors)
        # exit(0)
        
        errors = float(errors) / float(total2)
        ret += errors
        total += 1

        all_movies.reverse()
        final = []
        final_sorted = []
        for i in all_movies: 
            final.append([i, y_r[str(i)]])
            final_sorted.append([i, y_r[str(i)]])
        final_sorted = sorted(final_sorted, key=lambda x: x[1])
        final_sorted.reverse()

        # print(final)
        # print(final_sorted)
        # print()

        # Calculate NDCG
        for k in Ks:
            if k <= len(final):
                out = final[:k]
                out_sorted = final_sorted[:k]

                now = 0.0
                now_best = 0.0
                for i in range(k):
                    now += float(out[i][1]) / float(np.log2(i+2))
                    now_best += float(out_sorted[i][1]) / float(np.log2(i+2))

                NDCG[str(k)] += float(now) / float(now_best)
                total_NDCG[str(k)] += 1.0

    ret = float(ret) / float(total)
    ret *= 100.0
    ret = round(ret, 4)

    for k in NDCG:
        if total_NDCG[k] > 0: NDCG[k] = float(NDCG[k]) / float(total_NDCG[k])
        NDCG[k] *= 100.0
        NDCG[k] = round(NDCG[k], 4)

    return ret, NDCG

def evaluate(model, criterion, reader, hyper_params, is_train_set):
    model.eval()

    metrics = {}
    metrics['CP'] = 0.0
    # metrics['NCP'] = 0.0
    metrics['ZEROS'] = 0.0
    metrics['loss'] = 0.0

    first_indices = LongTensor(range(0, hyper_params['batch_size'], 2))
    second_indices = LongTensor(range(1, hyper_params['batch_size'], 2))

    correct = 0
    not_correct = 0
    zeros = 0
    total = 0
    batch = 0

    for x, y in reader.iter():
        batch += 1
        if batch > hyper_params['testing_batch_limit']: break

        output = model(x)

        metrics['loss'] += criterion(output, y).data

        output = output[:,0]
        output_first = output[first_indices]
        output_second = output[second_indices]

        y_first = y[first_indices]
        y_second = y[second_indices]

        temp_correct = 0
        temp_not_correct = 0
        temp_zeros = 0

        temp_correct += int(torch.sum(torch.lt(y_first, y_second) * torch.lt(output_first, output_second)))
        temp_correct += int(torch.sum(torch.gt(y_first, y_second) * torch.gt(output_first, output_second)))

        temp_not_correct += int(torch.sum(torch.lt(y_first, y_second) * torch.lt(output_second, output_first)))
        temp_not_correct += int(torch.sum(torch.gt(y_first, y_second) * torch.gt(output_second, output_first)))

        temp_zeros += int(torch.sum(torch.eq(output_second, output_first)))

        correct += temp_correct
        not_correct += temp_not_correct
        zeros += temp_zeros

        assert temp_correct + temp_not_correct + temp_zeros == int(y_first.shape[0])

        total += int(y_first.shape[0])

    assert correct + not_correct + zeros == total

    metrics['CP'] = float(correct) / float(total)
    metrics['CP'] *= 100.0
    metrics['CP'] = round(metrics['CP'], 4)

    # metrics['NCP'] = float(not_correct) / float(total)
    # metrics['NCP'] *= 100.0
    # metrics['NCP'] = round(metrics['NCP'], 4)

    metrics['ZEROS'] = float(zeros) / float(total)
    metrics['ZEROS'] *= 100.0
    metrics['ZEROS'] = round(metrics['ZEROS'], 4)

    metrics['loss'] = float(metrics['loss'][0]) / float(batch)
    metrics['loss'] = round(metrics['loss'], 4)

    if is_train_set == False:
        metrics['inversions'], ndcg = evaluate_inversions(model, criterion, reader, hyper_params, is_train_set)
        for k in ndcg: metrics['NDCG@' + str(k)] = ndcg[k]

    return metrics