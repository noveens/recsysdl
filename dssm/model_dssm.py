import math
import torch
import random
import numpy as np
from torch import nn
from torch.autograd import Variable
import torch.nn.functional as F

class Model(nn.Module):
    def __init__(self, hyper_params):
        super(Model, self).__init__()
        self.hyper_params = hyper_params

        self.user_embed = nn.Embedding(hyper_params['total_users'] + 1, hyper_params['user_embed_size'])
        self.item_embed = nn.Embedding(hyper_params['total_items'] + 1, hyper_params['item_embed_size'])

        self.user_rnn = nn.GRU(hyper_params['item_embed_size'], hyper_params['user_rnn_size'], hyper_params['user_rnn_layers'], dropout=hyper_params['user_rnn_dropout'], bidirectional=hyper_params['user_rnn_bi'], batch_first=True)
        self.item_rnn = nn.GRU(hyper_params['user_embed_size'], hyper_params['item_rnn_size'], hyper_params['item_rnn_layers'], dropout=hyper_params['item_rnn_dropout'], bidirectional=hyper_params['item_rnn_bi'], batch_first=True)

        if hyper_params['user_attention'] == True:
            # self.attention_weights = nn.Parameter(torch.FloatTensor(hyper_params['user_rnn_size']), requires_grad = True)
            self.attention_dense_layer = nn.Linear(hyper_params['user_rnn_size'], 1)
            self.attention_dropout = nn.Dropout(0.1)
            # self.attention_activation = nn.Tanh()
            self.attention_activation = nn.ReLU()
            self.softmax = nn.Softmax(1)

        self.activation = nn.ReLU()
        self.activation_last = nn.Sigmoid()
        
        self.layer1 = nn.Linear(hyper_params['user_rnn_size'], 32)
        self.drop1  = nn.Dropout(0.2)
        self.layer2 = nn.Linear(32, 16)
        self.drop2  = nn.Dropout(0.2)
        self.layer3 = nn.Linear(16, 1)

    def init_hidden(self, batch_size, bi, layers, size):
        mul = 1
        if bi == True: mul = 2
        # return Variable(torch.zeros(mul * layers, batch_size, size).cuda(), requires_grad=True)
        return Variable(torch.randn(mul * layers, batch_size, size).cuda(), requires_grad=True)

    def repack(self, h):
        if type(h) == Variable:
            return Variable(h.data.cuda(), requires_grad=True)
        else:
            return tuple(repackage_hidden(v) for v in h)

    def attention2(self, hidden_states):
        scores = self.attention_activation(hidden_states.matmul(self.attention_weights))
        scores = self.softmax(scores)
        # print(scores)
        # print(torch.sum(scores, 1))
        weighted = torch.mul(hidden_states, scores.unsqueeze(-1).expand_as(hidden_states))
        # print(weighted.sum(1).squeeze())
        # print("\n\n")
        return weighted.sum(1).squeeze()

    def attention(self, hidden_states):
        shape = hidden_states.shape
        scores = self.attention_dropout(self.attention_activation(self.attention_dense_layer(hidden_states.contiguous().view(-1, shape[-1])).view(-1, shape[1])))
        scores = self.softmax(scores)
        # print(scores)
        # print(torch.sum(scores, 1))
        return scores.unsqueeze(1).bmm(hidden_states).squeeze(1)
        # print(final)
        # print("\n\n")

    def forward(self, src_user_hist, src_item_hist):

        user_hist = self.item_embed(src_user_hist)          # [bsz x history_len x user_embed_size]
        
        item_hist = self.user_embed(src_item_hist)          # [bsz x history_len x item_embed_size]

        # self.user_rnn_hidden = self.repack(self.user_rnn_hidden)
        self.user_rnn_hidden = self.init_hidden(user_hist.size(0), self.hyper_params['user_rnn_bi'], self.hyper_params['user_rnn_layers'], self.hyper_params['user_rnn_size'])
        self.user_rnn.flatten_parameters()
        user_rnn_output, self.user_rnn_hidden = self.user_rnn(user_hist, self.user_rnn_hidden) # [bsz x user_rnn_size]
        
        # self.item_rnn_hidden = self.repack(self.item_rnn_hidden)
        self.item_rnn_hidden = self.init_hidden(item_hist.size(0), self.hyper_params['item_rnn_bi'], self.hyper_params['item_rnn_layers'], self.hyper_params['item_rnn_size'])
        self.item_rnn.flatten_parameters()
        item_rnn_output, self.item_rnn_hidden = self.item_rnn(item_hist, self.item_rnn_hidden) # [bsz x item_rnn_size]

        if self.hyper_params['user_attention'] == True: user_attention_output = self.attention(user_rnn_output)
        else: user_attention_output = user_rnn_output[:, -1, :]

        if self.hyper_params['item_attention'] == True: item_attention_output = self.attention(item_rnn_output)
        else: item_attention_output = item_rnn_output[:, -1, :]

        same_user = self.drop1(self.activation(self.layer1(user_attention_output)))
        same_item = self.drop1(self.activation(self.layer1(item_attention_output)))

        cross = same_user * same_item

        output = self.drop2(self.activation(self.layer2(cross)))
        output = self.activation_last(self.layer3(output))

        return output # [bsz x 1]